import {
  EVENT_FETCHED,
  LOAD_EVENT,
  LOAD_EVENTS,
  SET_EVENTS_DATE_FILTER,
  SET_EVENTS_PLACE_ID_FILTER,
  FILTERED_EVENTS_FETCHED,
  EVENT_NOT_FOUND,
  loadEvent,
  loadEvents,
  eventFetched,
  setEventsDateFilter,
  setEventsPlaceIdFilter,
  filteredEventsFetched,
  eventNotFound,
} from '../../../src/state/actions/events';

describe('events actions', () => {
  it('should create an action for FILTERED_EVENTS_FETCHED', () => {
    const events = [
      {
        id: 1,
        title: 'Test'
      }
    ];
    const expectedAction = {
      type: FILTERED_EVENTS_FETCHED,
      payload: {
        response: {
          data: events
        },
        request: {
          filters: {
            placeId: 1
          }
        }
      }
    };

    expect(filteredEventsFetched({ data: events }, { filters: { placeId: 1 } })).toEqual(expectedAction);
  });

  it('should create an action for EVENT_FETCHED', () => {
    const event = {
      id: 1,
      title: 'Test'
    };
    const expectedAction = {
      type: EVENT_FETCHED,
      payload: {
        response: {
          data: event
        }
      }
    };

    expect(eventFetched({ data: event })).toEqual(expectedAction);
  });

  it('should create an action for EVENT_NOT_FOUND', () => {
    const expectedAction = {
      type: EVENT_NOT_FOUND,
      payload: {
        request: {
          id: 1
        }
      }
    };

    expect(eventNotFound({ id: 1 })).toEqual(expectedAction);
  });

  it('should create an action for LOAD_EVENT', () => {
    const expectedAction = {
      type: LOAD_EVENT,
      payload: {
        id: 1,
        requestId: 'load_event'
      }
    };

    expect(loadEvent(1, 'load_event')).toEqual(expectedAction);
  });

  it('should create an action for LOAD_EVENTS', () => {
    const expectedAction = {
      type: LOAD_EVENTS,
      payload: {
        requestId: 'load_events'
      }
    };

    expect(loadEvents('load_events')).toEqual(expectedAction);
  });

  it('should create an action SET_EVENTS_PLACE_ID_FILTER', () => {
    const placeId = 1;
    const expectedAction = {
      type: SET_EVENTS_PLACE_ID_FILTER,
      payload: {
        placeId
      }
    };

    expect(setEventsPlaceIdFilter(placeId)).toEqual(expectedAction);
  });

  it('should create an action SET_EVENTS_DATE_FILTER', () => {
    const date = '10/18/2018';
    const expectedAction = {
      type: SET_EVENTS_DATE_FILTER,
      payload: {
        date
      }
    };

    expect(setEventsDateFilter(date)).toEqual(expectedAction);
  });
});
