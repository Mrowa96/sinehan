import { LOAD_TICKETS, TICKETS_FETCHED, loadTickets, ticketsFetched } from "../../../src/state/actions/tickets";
import { instanceOf } from "prop-types";

describe('tickets actions', () => {
  it('should create an action TICKETS_FETCHED', () => {
    const tickets = [
      {
        id: 1,
        title: 'Test'
      }
    ];
    const expectedAction = {
      type: TICKETS_FETCHED,
      payload: {
        response: {
          data: tickets
        }
      }
    };

    expect(ticketsFetched({ data: tickets })).toEqual(expectedAction);
  });

  it('should create an action LOAD_TICKETS', () => {
    const expectedAction = {
      type: LOAD_TICKETS,
      payload: {
        requestId: 'load_tickets',
        forceOverride: true
      }
    };

    expect(loadTickets('load_tickets', true)).toEqual(expectedAction);
  });

  it('should create an action LOAD_TICKETS with default parameters', () => {
    const result = loadTickets();

    expect(result.type).toEqual('LOAD_TICKETS');
    expect(result.payload.forceOverride).toEqual(false);
  });
});
