import reducers from '../../../src/state/reducers/index';

describe('places reducers', () => {
  it('should handle PLACES_FETCHED action', () => {
    const state = reducers(
        { places: { byId: {} } },
        {
          type: 'PLACES_FETCHED',
          payload: {
            response: {
              data: [
                {
                  id: 1,
                  name: 'Cracow',
                },
                {
                  id: 2,
                  name: 'Warsaw',
                },
              ],
            },
          },
        },
      ),
      expectedState = {
        byId: {
          1: {
            id: 1,
            name: 'Cracow',
          },
          2: {
            id: 2,
            name: 'Warsaw',
          },
        },
      };

    expect(state.places).toEqual(expectedState);
  });
});
