import reducers from '../../../src/state/reducers/index';
import { initialState } from '../../../src/state/store/initialState';

describe('reservation reducers', () => {
  it('should handle CHANGE_TICKET_QUANTITY action with empty tickets array', () => {
    const state = reducers(
        { reservation: { tickets: [], seats: [] } },
        {
          type: 'CHANGE_TICKET_QUANTITY',
          payload: {
            ticketId: 1,
            quantity: 1,
          },
        },
      ),
      expectedState = {
        tickets: [
          {
            id: 1,
            quantity: 1,
          },
        ],
        seats: [],
      };

    expect(state.reservation).toEqual(expectedState);
  });

  it('should handle CHANGE_TICKET_QUANTITY action and change quantity of existing object', () => {
    const state = reducers(
        { reservation: { tickets: [{ id: 1, quantity: 2 }], seats: [] } },
        {
          type: 'CHANGE_TICKET_QUANTITY',
          payload: {
            ticketId: 1,
            quantity: 1,
          },
        },
      ),
      expectedState = {
        tickets: [
          {
            id: 1,
            quantity: 1,
          },
        ],
        seats: [],
      };

    expect(state.reservation).toEqual(expectedState);
  });

  it('should handle CHANGE_TICKET_QUANTITY action and remove existing object', () => {
    const state = reducers(
        { reservation: { tickets: [{ id: 1, quantity: 2 }], seats: [] } },
        {
          type: 'CHANGE_TICKET_QUANTITY',
          payload: {
            ticketId: 1,
            quantity: 0,
          },
        },
      ),
      expectedState = {
        tickets: [],
        seats: [],
      };

    expect(state.reservation).toEqual(expectedState);
  });

  it('should handle CHANGE_TICKET_QUANTITY action and clear seats array', () => {
    const state = reducers(
        { reservation: { tickets: [{ id: 1, quantity: 2 }], seats: [{ row: 1, column: 2 }] } },
        {
          type: 'CHANGE_TICKET_QUANTITY',
          payload: {
            ticketId: 1,
            quantity: 3,
          },
        },
      ),
      expectedState = {
        tickets: [
          {
            id: 1,
            quantity: 3,
          },
        ],
        seats: [],
      };

    expect(state.reservation).toEqual(expectedState);
  });

  it('should handle SET_RESERVATION_PARAMETERS action', () => {
    const state = reducers(
        { reservation: { ...initialState.reservation } },
        {
          type: 'SET_RESERVATION_PARAMETERS',
          payload: {
            parameters: {
              eventId: 1,
              showId: 1,
              place: {
                id: 1,
                name: 'Test place',
                room: {
                  id: 1,
                  name: 'Test room',
                },
              },
              date: '2018-11-20',
              startTime: '20:00',
              takenSeats: [{ column: 1, row: 2 }],
            },
          },
        },
      ),
      expectedState = {
        ...initialState.reservation,
        eventId: 1,
        showId: 1,
        place: {
          id: 1,
          name: 'Test place',
          room: {
            id: 1,
            name: 'Test room',
          },
        },
        date: '2018-11-20',
        startTime: '20:00',
        tickets: [],
        takenSeats: [{ column: 1, row: 2 }],
      };

    expect(state.reservation).toEqual(expectedState);
  });

  it('should handle CLEAR_RESERVATION action', () => {
    const state = reducers(
      {
        reservation: {
          eventId: 1,
          showId: 1,
          place: {
            id: 1,
            name: 'Test place',
            room: {
              id: 1,
              name: 'Test room',
            },
          },
          date: '2018-11-20',
          startTime: '20:00',
          tickets: [{ id: 1, quantity: 2 }],
          clientData: { name: 'test', surname: 'test', email: 'test@test.pl', phone: 123456778 },
        },
      },
      {
        type: 'CLEAR_RESERVATION',
        payload: {},
      },
    );

    expect(state.reservation).toEqual(initialState.reservation);
  });

  it('should handle UPDATE_CLIENT_DATA action', () => {
    const state = reducers(
        { reservation: { clientData: { name: '', surname: '', email: '', phone: '' } } },
        {
          type: 'UPDATE_CLIENT_DATA',
          payload: {
            property: 'name',
            value: 'test',
          },
        },
      ),
      expectedState = {
        clientData: {
          name: 'test',
          surname: '',
          email: '',
          phone: '',
        },
      };

    expect(state.reservation).toEqual(expectedState);
  });

  it('should handle RESTORE_RESERVATION action', () => {
    const reservation = {
      id: 1,
      eventId: 1,
      showId: 1,
      place: {},
      date: '2019-01-01',
      startTime: '17:00',
    };
    const state = reducers(
        { reservation: { ...initialState.reservation } },
        {
          type: 'RESTORE_RESERVATION',
          payload: {
            reservation,
          },
        },
      ),
      expectedState = {
        ...initialState.reservation,
        ...reservation,
        fromStorage: true,
      };

    expect(state.reservation).toEqual(expectedState);
  });
});
