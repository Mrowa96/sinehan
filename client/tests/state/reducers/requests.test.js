import reducers from '../../../src/state/reducers/index';
import { ADD_REQUEST, UPDATE_REQUEST } from '../../../src/state/actions/requests';
import RequestStatus from '../../../src/models/RequestStatus';

describe('request reducers', () => {
  it('should handle ADD_REQUEST action', () => {
    const requestId = Symbol('request');
    const method = 'GET';
    const endpoint = 'endpoint';
    const filters = {
      limit: 1,
    };
    const data = {
      id: 1,
    };
    const state = reducers(
        { requests: {} },
        {
          type: ADD_REQUEST,
          payload: {
            requestId,
            method,
            endpoint,
            filters,
            data,
          },
        },
      ),
      expectedEventsState = {
        [requestId]: {
          method,
          endpoint,
          filters,
          data,
          status: RequestStatus.PENDING,
        },
      };

    expect(state.requests).toEqual(expectedEventsState);
  });

  it('should handle UPDATE_REQUEST action', () => {
    const requestId = Symbol('request');
    const method = 'GET';
    const endpoint = 'endpoint';
    const filters = {
      limit: 1,
    };
    const initialState = {
      requests: {
        [requestId]: {
          method,
          endpoint,
          filters,
          status: RequestStatus.PENDING,
        },
      },
    };
    const state = reducers(initialState, {
        type: UPDATE_REQUEST,
        payload: {
          requestId,
          data: {
            status: RequestStatus.FAIL,
          },
        },
      }),
      expectedEventsState = {
        [requestId]: {
          method,
          endpoint,
          filters,
          status: RequestStatus.FAIL,
        },
      };

    expect(state.requests).toEqual(expectedEventsState);
  });
});
