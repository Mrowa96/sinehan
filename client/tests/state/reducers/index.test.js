import reducers from '../../../src/state/reducers/index';
import { initialState } from '../../../src/state/store/initialState';

describe('index reducers', () => {
  it('should return initial state', () => {
    expect(reducers(undefined, {})).toEqual(initialState);
  });
});
