import { put } from "redux-saga/effects";
import { loadTickets } from "../../../src/state/sagas/tickets";
import { ticketsFetched } from "../../../src/state/actions/tickets";
import { requestGet } from "../../../src/state/actions/requests";

describe('tickets saga', () => {
  it('should handle LOAD_TICKETS action when forceOverride is false and there are no tickets', () => {
    const generator = loadTickets({
      payload: {
        requestId: 'request',
        forceOverride: false
      }
    });
    const state = {
      tickets: {
        byId: null
      }
    };

    generator.next();

    expect(generator.next(state).value).toEqual(put(requestGet(`ticket`, {}, 'request', ticketsFetched)));
  });

  it('should handle LOAD_TICKETS action when forceOverride is true and there are some tickets', () => {
    const generator = loadTickets({
      payload: {
        requestId: 'request',
        forceOverride: true
      }
    });
    const state = {
      tickets: {
        byId: [
          {
            id: 1,
            name: 'ticket'
          }
        ]
      }
    };

    generator.next();

    expect(generator.next(state).value).toEqual(put(requestGet(`ticket`, {}, 'request', ticketsFetched)));
  });

  it('should handle LOAD_TICKETS action when forceOverride is false and there are some tickets', () => {
    const generator = loadTickets({
      payload: {
        requestId: 'request',
        forceOverride: false
      }
    });
    const state = {
      tickets: {
        byId: [
          {
            id: 1,
            name: 'ticket'
          }
        ]
      }
    };

    generator.next();

    expect(generator.next(state).done).toEqual(true);
  });

  it('should handle LOAD_TICKETS action when forceOverride is true and there are some tickets', () => {
    const generator = loadTickets({
      payload: {
        requestId: 'request',
        forceOverride: true
      }
    });
    const state = {
      tickets: {
        byId: null
      }
    };

    generator.next();

    expect(generator.next(state).value).toEqual(put(requestGet(`ticket`, {}, 'request', ticketsFetched)));
  });
});
