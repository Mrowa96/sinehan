import { put, call } from 'redux-saga/effects';
import RequestStatus from '../../../src/models/RequestStatus';
import { apiGet, apiDelete, apiPost, apiPut } from '../../../src/utils/request';
import {
  requestGet,
  requestPost,
  requestFailed,
  requestSucceed,
  requestPut,
  requestDelete,
} from '../../../src/state/sagas/requests';
import { addRequest, requestSucceed as requestSucceedAction, updateRequest } from '../../../src/state/actions/requests';

describe('request saga', () => {
  it('should handle REQUEST_GET action', () => {
    const succeedAction = () => {};
    const failedAction = () => {};
    const requestId = Symbol('request');
    const generator = requestGet({
      payload: {
        endpoint: 'endpoint',
        filters: {},
        requestId,
        succeedAction,
        failedAction,
      },
    });

    expect(generator.next().value).toEqual(put(addRequest(requestId, 'GET', 'endpoint', {})));
    expect(generator.next().value).toEqual(call(apiGet, 'endpoint', {}));
    expect(generator.next({ data: [] }).value).toEqual(
      put(requestSucceedAction({ data: [] }, requestId, succeedAction)),
    );
  });

  it('should handle REQUEST_DELETE action', () => {
    const succeedAction = () => {};
    const failedAction = () => {};
    const requestId = Symbol('request');
    const generator = requestDelete({
      payload: {
        endpoint: 'endpoint',
        filters: {},
        requestId,
        succeedAction,
        failedAction,
      },
    });

    expect(generator.next().value).toEqual(put(addRequest(requestId, 'DELETE', 'endpoint', {})));
    expect(generator.next().value).toEqual(call(apiDelete, 'endpoint', {}));
    expect(generator.next({ data: [] }).value).toEqual(
      put(requestSucceedAction({ data: [] }, requestId, succeedAction)),
    );
  });

  it('should handle REQUEST_POST action', () => {
    const succeedAction = () => {};
    const failedAction = () => {};
    const requestId = Symbol('request');
    const generator = requestPost({
      payload: {
        endpoint: 'endpoint',
        data: {},
        requestId,
        succeedAction,
        failedAction,
      },
    });

    expect(generator.next().value).toEqual(put(addRequest(requestId, 'POST', 'endpoint', {}, {})));
    expect(generator.next().value).toEqual(call(apiPost, 'endpoint', {}));
    expect(generator.next({ data: [] }).value).toEqual(
      put(requestSucceedAction({ data: [] }, requestId, succeedAction)),
    );
  });

  it('should handle REQUEST_PUT action', () => {
    const succeedAction = () => {};
    const failedAction = () => {};
    const requestId = Symbol('request');
    const generator = requestPut({
      payload: {
        endpoint: 'endpoint',
        data: { id: 1 },
        requestId,
        succeedAction,
        failedAction,
      },
    });

    expect(generator.next().value).toEqual(put(addRequest(requestId, 'PUT', 'endpoint', {}, { id: 1 })));
    expect(generator.next().value).toEqual(call(apiPut, 'endpoint', { id: 1 }));
    expect(generator.next({ data: [] }).value).toEqual(
      put(requestSucceedAction({ data: [] }, requestId, succeedAction)),
    );
  });

  it('should handle REQUEST_SUCCEED action', () => {
    const actionToDispatch = (a, b) => {};
    const generator = requestSucceed({
      payload: {
        response: {
          data: [],
        },
        requestId: 'request',
        actionToDispatch,
      },
    });

    generator.next();
    //generator.next({requests: {}});

    //expect(generator.next().value).toEqual(put(updateRequest('request', {status: RequestStatus.SUCCESS})));
  });

  it('should handle REQUEST_SUCCEED action without actionToDispatch', () => {
    const generator = requestSucceed({
      payload: {
        response: {
          data: [],
        },
        requestId: 'request',
      },
    });

    generator.next();
    expect(generator.next().value).toEqual(put(updateRequest('request', { status: RequestStatus.SUCCESS })));
  });

  it('should handle REQUEST_FAILED action', () => {
    const actionToDispatch = (a, b) => {};
    const generator = requestFailed({
      payload: {
        requestId: 'request',
        actionToDispatch,
      },
    });

    generator.next();
    //generator.next({requests: {}});

    //expect(generator.next().value).toEqual(put(updateRequest('request', {status: RequestStatus.FAIL})));
  });

  it('should handle REQUEST_FAILED action without actionToDispatch', () => {
    const generator = requestFailed({
      payload: {
        requestId: 'request',
      },
    });

    generator.next();
    expect(generator.next().value).toEqual(put(updateRequest('request', { status: RequestStatus.FAIL })));
  });
});
