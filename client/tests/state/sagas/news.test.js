import { put } from 'redux-saga/effects';
import { serializeFilters } from '../../../src/utils/serializeFilters';
import { loadNews, loadOneNews } from '../../../src/state/sagas/news';
import { filteredNewsFetched, newsNotFound, oneNewsFetched } from '../../../src/state/actions/news';
import { requestGet } from '../../../src/state/actions/requests';

describe('news saga', () => {
  it('should handle LOAD_NEWS action without cache', () => {
    const state = {
      news: {
        byId: {},
        dataByFilter: {},
      },
    };
    const filters = {
      limit: 2,
    };
    const generator = loadNews({
      payload: {
        requestId: 'request',
        filters,
      },
    });

    generator.next();

    expect(generator.next(state).value).toEqual(put(requestGet('news', filters, 'request', filteredNewsFetched)));
  });

  it('should handle LOAD_NEWS action with cache', () => {
    const filters = {};
    const state = {
      news: {
        byId: {
          1: {
            id: 1,
            title: 'Test',
          },
        },
        dataByFilter: {
          [serializeFilters(filters)]: {
            ids: [1],
            totalQuantity: 1,
          },
        },
      },
    };
    const generator = loadNews({
      payload: {
        requestId: 'request',
        filters,
      },
    });

    generator.next();

    expect(generator.next(state).done).toEqual(true);
  });

  it('should handle LOAD_NEWS action with cache even if it is empty', () => {
    const filters = {};
    const state = {
      news: {
        byId: {},
        dataByFilter: {
          [serializeFilters(filters)]: {
            ids: [],
            totalQuantity: 0,
          },
        },
      },
    };
    const generator = loadNews({
      payload: {
        requestId: 'request',
        filters,
      },
    });

    generator.next();

    expect(generator.next(state).done).toEqual(true);
  });

  it('should handle LOAD_ONE_NEWS action with existing news', () => {
    const existingNews = {
      id: 1,
      title: 'Test',
    };
    const state = {
      news: {
        byId: {
          [existingNews.id]: existingNews,
        },
      },
    };
    const generator = loadOneNews({
      payload: {
        id: 1,
        requestId: 'request',
      },
    });

    generator.next();

    expect(generator.next(state).value).toEqual(put(oneNewsFetched({ data: existingNews })));
  });

  it('should handle LOAD_ONE_NEWS action with missing news', () => {
    const state = {
      news: {
        byId: {},
      },
    };
    const generator = loadOneNews({
      payload: {
        id: 1,
        requestId: 'request',
      },
    });

    generator.next();

    expect(generator.next(state).value).toEqual(
      put(requestGet(`news/1`, { id: 1 }, 'request', oneNewsFetched, newsNotFound)),
    );
  });
});
