import { put } from "redux-saga/effects";
import { loadPlaces } from "../../../src/state/sagas/places";
import { placesFetched } from "../../../src/state/actions/places";
import { requestGet } from "../../../src/state/actions/requests";

describe('places saga', () => {
  it('should handle LOAD_PLACES action', () => {
    const generator = loadPlaces({
      payload: {
        requestId: 'request'
      }
    });

    expect(generator.next().value).toEqual(put(requestGet(`place`, {}, 'request', placesFetched)));
  });
});
