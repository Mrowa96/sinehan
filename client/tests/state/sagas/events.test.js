import { loadEvent, loadEvents } from '../../../src/state/sagas/events';
import { eventFetched, eventNotFound, filteredEventsFetched } from '../../../src/state/actions/events';
import { put } from 'redux-saga/effects';
import { requestGet } from '../../../src/state/actions/requests';
import { serializeFilters } from '../../../src/utils/serializeFilters';

describe('events saga', () => {
  it('should handle LOAD_EVENT action with existing event', () => {
    const existingEvent = {
      id: 1,
      title: 'Test',
    };
    const state = {
      events: {
        byId: {
          [existingEvent.id]: existingEvent,
        },
      },
    };
    const generator = loadEvent({
      payload: {
        id: 1,
        requestId: 'request',
      },
    });

    generator.next();

    expect(generator.next(state).value).toEqual(put(eventFetched({ data: existingEvent })));
  });

  it('should handle LOAD_EVENT action with missing event', () => {
    const state = {
      events: {
        byId: {},
      },
    };
    const generator = loadEvent({
      payload: {
        id: 1,
        requestId: 'request',
      },
    });

    generator.next();

    expect(generator.next(state).value).toEqual(
      put(requestGet(`event/1`, { id: 1 }, 'request', eventFetched, eventNotFound)),
    );
  });

  it('should handle LOAD_EVENTS action with filters and without cache', () => {
    const state = {
      events: {
        byId: {},
        dataByFilter: {},
      },
    };
    const filters = {
      placeId: 1,
      date: null,
    };
    const generator = loadEvents({
      payload: {
        requestId: 'request',
        filters,
      },
    });

    generator.next();

    expect(generator.next(state).value).toEqual(put(requestGet('event', filters, 'request', filteredEventsFetched)));
  });

  it('should handle LOAD_EVENTS action with filters and cache', () => {
    const filters = {
      placeId: 1,
      date: null,
    };
    const state = {
      events: {
        byId: {
          1: {
            id: 1,
            title: 'Test',
            shows: [],
          },
        },
        dataByFilter: {
          [serializeFilters(filters)]: {
            ids: [1],
            totalQuantity: 1,
          },
        },
      },
    };
    const generator = loadEvents({
      payload: {
        requestId: 'request',
        filters,
      },
    });

    generator.next();

    expect(generator.next(state).done).toEqual(true);
  });

  it('should handle LOAD_EVENTS action without filters', () => {
    const state = {
      events: {
        byId: {
          1: {
            id: 1,
            title: 'Test',
          },
          2: {
            id: 2,
            title: 'Test 2',
          },
        },
        dataByFilter: {},
      },
    };
    const generator = loadEvents({
      payload: {
        requestId: 'request',
        filters: null,
      },
    });

    generator.next();

    expect(generator.next(state).value).toEqual(put(requestGet('event', null, 'request', filteredEventsFetched)));
  });
});
