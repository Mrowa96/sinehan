import canDisplayInitializePage from "../../src/guards/canDisplayInitializePage";

describe('test canDisplayInitializePage function', () => {
  it('should return true value', () => {
    expect(canDisplayInitializePage({
      reservation: {
        id: null,
        showId: 2,
        startTime: '08:00',
      }
    })).toEqual(true);
  });

  it('should return false value because there is no showId', () => {
    expect(canDisplayInitializePage({
      reservation: {
        id: null,
        showId: null,
        startTime: '08:00',
      }
    })).toEqual(false);
  });

  it('should return false value because there is no startTime', () => {
    expect(canDisplayInitializePage({
      reservation: {
        id: null,
        showId: 1,
        startTime: null
      }
    })).toEqual(false);
  });

  it('should return false value because there is reservation id', () => {
    expect(canDisplayInitializePage({
      reservation: {
        id: 2,
        showId: 1,
        startTime: '08:00'
      }
    })).toEqual(false);
  });
});
