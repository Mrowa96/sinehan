import canDisplayRestorePage from '../../src/guards/canDisplayRestorePage';

describe('test canDisplayRestorePage function', () => {
  const reservation = {
    id: 1,
    eventId: 1,
    fromStorage: true,
  };
  const tickets = { byId: { 1: { id: 1, price: 10, name: 'Test ticket' } } };
  const events = {
    byId: {
      1: {
        id: 1,
        name: 'Test event',
        shows: [],
      },
    },
  };

  test.each([
    {
      events: { ...events },
      reservation: { ...reservation, id: null },
      tickets: { ...tickets },
    },
    {
      events: { ...events },
      reservation: { ...reservation, eventId: null },
      tickets: { ...tickets },
    },
    {
      events: { ...events },
      reservation: { ...reservation, fromStorage: false },
      tickets: { ...tickets },
    },
    {
      events: { ...events },
      reservation: { ...reservation },
      tickets: { ...tickets },
    },
  ])('should return false', state => {
    expect(canDisplayRestorePage(state)).toBeFalsy();
  });

  test.each([
    {
      events: { ...events },
      reservation: { ...reservation },
      tickets: { byId: null },
    },
    {
      events: { byId: {} },
      reservation: { ...reservation },
      tickets: { ...tickets },
    },
  ])('should return true', state => {
    expect(canDisplayRestorePage(state)).toBeTruthy();
  });
});
