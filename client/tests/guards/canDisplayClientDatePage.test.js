import canDisplayClientDataPage from "../../src/guards/canDisplayClientDataPage";

describe('test canDisplayClientDatePage function', () => {
  it('should return true value', () => {
    expect(canDisplayClientDataPage({
      reservation: {
        eventId: 1,
        showId: 1,
        tickets: [
          {}, {}
        ],
        seats: [
          {}, {}
        ]
      }
    })).toEqual(true);
  });

  it('should return false value - empty tickets array', () => {
    expect(canDisplayClientDataPage({
      reservation: {
        eventId: 1,
        showId: 1,
        tickets: [],
        seats: [
          {}, {}
        ]
      }
    })).toEqual(false);
  });

  it('should return false value - event not selected', () => {
    expect(canDisplayClientDataPage({
      reservation: {
        eventId: null,
        showId: 1,
        tickets: [
          {}, {}
        ],
        seats: [
          {}, {}
        ]
      }
    })).toEqual(false);
  });

  it('should return false value - empty seats array', () => {
    expect(canDisplayClientDataPage({
      reservation: {
        eventId: 1,
        showId: 1,
        tickets: [
          {}, {}
        ],
        seats: []
      }
    })).toEqual(false);
  });
});
