import React from 'react';
import { mount } from 'enzyme';
import styles from '../../../../src/components/Reservation/ClientDataManager/ClientDataManager.module.scss';
import Button from '../../../../src/components/Button/Button';
import ClientDataManager from '../../../../src/components/Reservation/ClientDataManager/ClientDataManager';

describe('<ClientDataManager/>', () => {
  let component,
    reservation,
    onNextButtonClick,
    onPrevButtonClick,
    onClientDataChange,
    onAgreementChange,
    onClientDataSave;

  beforeEach(() => {
    onNextButtonClick = jest.fn();
    onPrevButtonClick = jest.fn();
    onClientDataChange = jest.fn();
    onAgreementChange = jest.fn();
    onClientDataSave = jest.fn();
    reservation = {
      clientData: {
        name: '',
        surname: '',
        email: '',
        phone: '',
      },
      agreements: {
        dataProcessing: false,
      },
    };

    component = mount(
      <ClientDataManager
        reservation={reservation}
        onNextButtonClick={onNextButtonClick}
        onPrevButtonClick={onPrevButtonClick}
        onClientDataChange={onClientDataChange}
        onAgreementChange={onAgreementChange}
        triggerClientDataSave={onClientDataSave}
      />,
    );
  });

  it('should have form element', () => {
    expect(component.find('form') !== null).toBeTruthy();
  });

  it('should invoke correct methods when user changes input values', () => {
    component.find('input[name="name"]').simulate('change', { target: { value: 'Some name' } });
    component.find('input[name="surname"]').simulate('change', { target: { value: 'Some surname' } });
    component.find('input[name="email"]').simulate('change', { target: { value: 'email@email.com' } });
    component.find('input[name="phone"]').simulate('change', { target: { value: '111 222 333' } });
    component.find('input[name="data-processing-agreement"]').simulate('change', { target: { checked: true } });

    expect(onClientDataChange.mock.calls.length).toBe(4);
    expect(onAgreementChange.mock.calls.length).toBe(1);
  });

  it('should not invoke onNextButtonClick when clicking on next button when form is invalid', () => {
    component
      .find(`.${styles.buttons}`)
      .find(Button)
      .last()
      .simulate('click');

    expect(onNextButtonClick.mock.calls.length).toBe(0);
  });

  it('should invoke onNextButtonClick when clicking on next button when form is valid', () => {
    reservation = {
      clientData: {
        name: 'test',
        surname: 'test',
        email: 'test@test.pl',
        phone: '123 123 123',
      },
      agreements: {
        dataProcessing: true,
      },
    };

    component.setProps({ reservation });
    component
      .find(`.${styles.buttons}`)
      .find(Button)
      .last()
      .simulate('click');

    expect(onNextButtonClick.mock.calls.length).toBe(1);
  });

  it('should invoke onPrevButtonClick when clicking on prev button', () => {
    component
      .find(`.${styles.buttons}`)
      .find(Button)
      .first()
      .simulate('click');

    expect(onPrevButtonClick.mock.calls.length).toBe(1);
  });
});
