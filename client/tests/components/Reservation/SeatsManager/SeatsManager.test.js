import React from 'react';
import { shallow } from 'enzyme';
import SeatsManager from '../../../../src/components/Reservation/SeatsManager/SeatsManager';
import styles from '../../../../src/components/Reservation/SeatsManager/SeatsManager.module.scss';
import Button from '../../../../src/components/Button/Button';

describe('<SeatsManager/>', () => {
  let component, room, reservation, onNextButtonClick, onPrevButtonClick, onSeatClick, totalTicketsQuantity;

  beforeEach(() => {
    onNextButtonClick = jest.fn();
    onPrevButtonClick = jest.fn();
    onSeatClick = jest.fn();
    totalTicketsQuantity = 2;
    room = {
      seats: {
        rows: 8,
        columns: 8,
      },
    };
    reservation = {
      seats: [],
    };

    component = shallow(
      <SeatsManager
        room={room}
        reservation={reservation}
        onNextButtonClick={onNextButtonClick}
        onPrevButtonClick={onPrevButtonClick}
        onSeatClick={onSeatClick}
        totalTicketsQuantity={totalTicketsQuantity}
      />,
    );
  });

  it('should next button be disable when selectedTickets is not equal to reservation seats', () => {
    let button = component
      .find(`.${styles.buttons}`)
      .find(Button)
      .last();

    expect(button.prop('disabled')).toBeTruthy();
  });

  it('should next button be enabled when selectedTickets is equal to reservation seats', () => {
    reservation.seats = [
      {
        column: 1,
        row: 1,
      },
      {
        column: 2,
        row: 1,
      },
    ];
    component.setProps({ reservation });
    let button = component
      .find(`.${styles.buttons}`)
      .find(Button)
      .last();

    expect(button.prop('disabled')).toBeFalsy();
  });

  it('should invoke onNextButtonClick when clicking on next button', () => {
    component
      .find(`.${styles.buttons}`)
      .find(Button)
      .last()
      .simulate('click');

    expect(onNextButtonClick.mock.calls.length).toBe(1);
  });

  it('should invoke onPrevButtonClick when clicking on prev button', () => {
    component
      .find(`.${styles.buttons}`)
      .find(Button)
      .first()
      .simulate('click');

    expect(onPrevButtonClick.mock.calls.length).toBe(1);
  });
});
