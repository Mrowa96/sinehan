import React from 'react';
import { shallow } from 'enzyme';
import Layout from '../../../src/components/Layout/Layout';
import Header from '../../../src/components/Layout/Header/Header';
import Navigation from '../../../src/components/Layout/Navigation/Navigation';
import BackToReservationButtonContainer from '../../../src/containers/BackToReservationButtonContainer';

describe('<Layout/>', () => {
  let component;

  beforeAll(() => {
    component = shallow(<Layout />);
  });

  it('should render Header', () => {
    expect(component.find(Header) !== null).toBeTruthy();
  });

  it('should render Navigation', () => {
    expect(component.find(Navigation) !== null).toBeTruthy();
  });

  it('should render BackToReservationButtonContainer', () => {
    expect(component.find(BackToReservationButtonContainer) !== null).toBeTruthy();
  });

  it('should render children in main section', () => {
    component = shallow(
      <Layout>
        <p>test</p>
      </Layout>,
    );

    expect(
      component
        .find('main')
        .children()
        .html(),
    ).toEqual('<p>test</p>');
  });
});
