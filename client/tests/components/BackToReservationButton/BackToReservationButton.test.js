import React from 'react';
import { shallow } from 'enzyme';
import { BackToReservationButton } from '../../../src/components/BackToReservationButton/BackToReservationButton';
import Button from '../../../src/components/Button/Button';

describe('<BackToReservationButton/>', () => {
  let component, route, history;

  beforeAll(() => {
    history = {
      push: jest.fn(),
    };
    route = {
      path: '/test',
    };
    component = shallow(<BackToReservationButton route={route} history={history} />);
  });

  it('update history on click', () => {
    component.find(Button).simulate('click');

    expect(history.push.mock.calls.length === 1).toBeTruthy();
    expect(history.push.mock.calls[0][0] === route.path).toBeTruthy();
  });
});
