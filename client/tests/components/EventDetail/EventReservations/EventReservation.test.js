import React from 'react';
import { shallow } from 'enzyme';
import EventReservation from '../../../../src/components/EventDetail/EventReservations/EventReservations';
import MakeReservationButtonContainer from '../../../../src/containers/MakeReservationButtonContainer';
import moment from 'moment';

describe('<EventReservation/>', () => {
  let component, event;

  beforeEach(() => {
    const place = {
      id: 1,
      name: 'Some place',
      room: {
        id: 1,
        name: 'Test room',
        seats: {
          columns: 5,
          rows: 6,
        },
      },
    };

    event = {
      shows: [
        {
          id: 1,
          place,
          date: moment()
            .add(1, 'days')
            .format('YYYY-MM-DD'),
          startTimes: ['9:00', '11:00'],
        },
        {
          id: 2,
          place,
          date: moment()
            .add(1, 'days')
            .format('YYYY-MM-DD'),
          startTimes: ['13:00', '15:00'],
        },
      ],
    };

    component = shallow(<EventReservation event={event} />);
  });

  it('should not render anything', () => {
    event = {
      shows: [],
    };
    component = shallow(<EventReservation event={event} />);

    expect(component.html()).toEqual('');
  });

  it('should render buttons to make reservation', () => {
    expect(component.find(MakeReservationButtonContainer).length).toEqual(4);
  });
});
