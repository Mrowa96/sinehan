import React from 'react';
import { shallow } from 'enzyme';
import styles from '../../../../src/components/EventDetail/EventDetailsAndDescription/EventDetailsAndDescription.module.scss';
import EventDetailsAndDescription from '../../../../src/components/EventDetail/EventDetailsAndDescription/EventDetailsAndDescription';

describe('<EventDetailsAndDescription/>', () => {
  let component, event;

  beforeEach(() => {
    event = {
      description: 'Some not so long description',
      originalTitle: 'Original event title',
      directorName: 'John Smith',
      production: {
        countries: ['Poland', 'Ireland'],
        year: 2033,
      },
      ageLimit: 8,
      releaseDate: '2033-11-23',
      duration: 77,
    };
    component = shallow(<EventDetailsAndDescription event={event} />);
  });

  it('should render description', () => {
    expect(component.find(`.${styles.descriptionWrapper} p`).text()).toEqual(event.description);
  });

  it('should render original title in details', () => {
    expect(component.find(`.${styles.detailsWrapper}`)).toIncludeText(event.originalTitle);
  });

  it('should render director name in details', () => {
    expect(component.find(`.${styles.detailsWrapper}`)).toIncludeText(event.directorName);
  });

  it('should render production countries in details', () => {
    expect(component.find(`.${styles.detailsWrapper}`)).toIncludeText(event.production.countries[1]);
  });

  it('should render production year in details', () => {
    expect(component.find(`.${styles.detailsWrapper}`)).toIncludeText(event.production.year);
  });

  it('should render age limit in details', () => {
    expect(component.find(`.${styles.detailsWrapper}`)).toIncludeText(event.ageLimit);
  });

  it('should render release date in details', () => {
    expect(component.find(`.${styles.detailsWrapper}`)).toIncludeText(event.releaseDate);
  });

  it('should render duration in details', () => {
    expect(component.find(`.${styles.detailsWrapper}`)).toIncludeText(event.duration);
  });

  it('should render empty table', () => {
    event = {
      description: 'Some not so long description',
    };
    component = shallow(<EventDetailsAndDescription event={event} />);

    expect(component.find(`.${styles.detailsWrapper} table tbody`).text()).toEqual('');
  });
});
