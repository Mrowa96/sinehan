import { arrayToObject } from "../../src/utils/arrayToObject";

describe('test arrayToObject utility', () => {
  it('[arrayToObject] should return object from array', () => {
    const values = [
      {
        key: 1,
        name: 'test'
      },
      {
        key: 3,
        name: 'test 3'
      }
    ];
    const expectedValue = {
      1: {
        key: 1,
        name: 'test'
      },
      3: {
        key: 3,
        name: 'test 3'
      }
    };

    expect(arrayToObject(values, 'key')).toEqual(expectedValue);
  });
});
