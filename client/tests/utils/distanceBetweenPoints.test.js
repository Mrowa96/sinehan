import { distanceBetweenPoints } from '../../src/utils/distanceBetweenPoints';

describe('test distanceBetweenPoints utility', () => {
  it('[distanceBetweenPoints] should return correct value', () => {
    const a = {
      x: 2,
      y: 3,
    };
    const b = {
      x: 4,
      y: 5,
    };

    expect(distanceBetweenPoints(a, b)).toEqual(2.8284271247461903);
  });
});
