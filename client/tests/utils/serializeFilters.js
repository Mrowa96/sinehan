import { serializeFilters } from '../../src/utils/serializeFilters';

describe('test serializeFilters utility', () => {
  it('[serializeFilters] should return serialized filters string with placeId', () => {
    const filters = {
      placeId: 2,
      date: null,
    };

    expect(serializeFilters(filters)).toEqual('f_placeId_2_date_null_');
  });

  it('[serializeFilters] should return serialized filters string with date and placeId', () => {
    const filters = {
      placeId: 2,
      date: '2018-10-18',
    };

    expect(serializeFilters(filters)).toEqual('f_placeId_2_date_2018-10-18_');
  });

  it('[serializeFilters] should return serialized filters string with @', () => {
    const filters = {
      placeId: null,
      date: null,
    };

    expect(serializeFilters(filters)).toEqual('f_placeId_null_date_null_');
  });
});
