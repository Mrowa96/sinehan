import isValid from '../../src/utils/validateReservationFromStorage';

describe('test validateReservationFromStorage utility', () => {
  let reservation;

  beforeEach(() => {
    reservation = {
      id: 1,
      eventId: 1,
      showId: 1,
      place: {},
      date: '2019-01-01',
      startTime: '17:00',
    };
  });

  test.each([
    { ...reservation, id: null },
    { ...reservation, eventId: null },
    { ...reservation, showId: null },
    { ...reservation, place: null },
    { ...reservation, date: null },
    { ...reservation, startTime: null },
  ])('[isValid] should return false when some reservation parameter is missing', reservation => {
    expect(isValid(reservation)).toBeFalsy();
  });

  it('[isValid] should return false when show starts in less than 30 minutes', () => {
    expect(isValid(reservation, '2019-01-01 16:45')).toBeFalsy();
  });

  it('[isValid] should return true ', () => {
    expect(isValid(reservation, '2019-01-01 16:25')).toBeTruthy();
  });
});
