import { nearestPlaceId } from '../../src/utils/nearestPlace';

describe('test nearestPlaceId utility', () => {
  it('[nearestPlaceId] should return nearest place', () => {
    const currentCoords = {
      latitude: 2,
      longitude: 3,
    };
    const places = [
      {
        id: 1,
        coords: {
          latitude: 3,
          longitude: 4,
        },
      },
      {
        id: 2,
        coords: {
          latitude: 6,
          longitude: 6,
        },
      },
    ];

    expect(nearestPlaceId(currentCoords, places)).toEqual(1);
  });
});
