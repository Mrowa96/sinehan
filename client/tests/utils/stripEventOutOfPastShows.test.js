import { stripEventOutOfPastShows } from "../../src/utils/stripEventOutOfPastShows";

describe('test stripEventOutOfPastShows utility', () => {
  it('[stripEventOutOfPastShows] should return event without previous reservation times', () => {
    const event = {
      id: 1,
      title: 'Some event',
      shows: [
        {
          id: 1,
          date: "2018-11-30",
          startTimes: ["11:45", "14:10"],
        },
        {
          id: 2,
          date: "2018-11-31",
          startTimes: ["9:00", "11:00"]
        },
        {
          id: 3,
          date: "2018-12-01",
          startTimes: ["13:00", "15:00", "17:00"]
        },
        {
          id: 4,
          date: "2018-11-31",
          startTimes: ["7:00", "10:30"]
        },
        {
          id: 5,
          date: "2018-12-01",
          startTimes: ["12:00", "18:07"]
        },
        {
          id: 6,
          date: "2018-12-02",
          startTimes: ["16:00", "20:00"]
        },
      ]
    };
    const currentDateTime = "2018-12-01 14:32";

    const strippedEvent = {
      id: 1,
      title: "Some event",
      shows: [
        {
          id: 3,
          date: "2018-12-01",
          startTimes: ["17:00"]
        },
        {
          id: 5,
          date: "2018-12-01",
          startTimes: ["18:07"]
        },
        {
          id: 6,
          date: "2018-12-02",
          startTimes: ["16:00", "20:00"]
        },
      ],
    };

    expect(stripEventOutOfPastShows(event, currentDateTime)).toEqual(strippedEvent);
  });

  it('[stripEventOutOfPastShows] should return empty object if event has not got id', () => {
    expect(stripEventOutOfPastShows({})).toEqual({});
  });

  it('[stripEventOutOfPastShows] should return undefined if event is undefined', () => {
    expect(stripEventOutOfPastShows(undefined)).toEqual(undefined);
  });
});
