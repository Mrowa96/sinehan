import React from 'react';
import { mount } from 'enzyme';
import { createMockStore } from 'redux-test-utils';
import { InitializeConnected } from '../../../src/containers/Reservation/InitializeContainer';
import Initialize from '../../../src/components/Reservation/Initialize/Initialize';
import { CREATE_RESERVATION } from '../../../src/state/actions/reservation';

describe('InitializeContainer', () => {
  let store, component, history;

  beforeEach(() => {
    store = createMockStore({
      reservation: {
        showId: 1,
        startTime: '2018-10-12',
      },
      requests: {},
    });

    history = {
      goBack: jest.fn(),
    };

    component = mount(<InitializeConnected />, { context: { store } });
  });

  it('should always display Initialize component', () => {
    expect(component.find(Initialize)).toExist();
  });

  it('should dispatch createReservation action', () => {
    setTimeout(() => {
      expect(store.isActionTypeDispatched(CREATE_RESERVATION)).toBeTruthy();
    }, 3000);
  });

  it('should not dispatch createReservation action and should invoke goBack method on history object', () => {
    store = createMockStore({
      reservation: {
        showId: null,
        startTime: null,
      },
      requests: {},
    });

    component = mount(<InitializeConnected history={history} />, { context: { store } });

    expect(store.isActionTypeDispatched(CREATE_RESERVATION)).toBeFalsy();
    expect(history.goBack.mock.calls.length === 1).toBeTruthy();
  });
});
