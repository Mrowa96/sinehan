import { createPreserveStateMiddleware } from '../../src/state/middlewares/preserveStateMiddleware';
import config from '../../src/config';

describe('test preserveStateMiddleware middleware', () => {
  it('[createPreserveStateMiddleware] should call next function with action', () => {
    const middleware = createPreserveStateMiddleware();
    const storeMock = {
      getState: jest.fn(),
    };
    const nextMock = jest.fn();
    const action = { type: 'SOME_TYPE' };

    middleware(storeMock)(nextMock)(action);

    expect(nextMock.mock.calls.length).toEqual(1);
    expect(nextMock.mock.calls[0][0]).toEqual(action);
  });

  it('[createPreserveStateMiddleware] should save placeId filter', () => {
    const storageMock = {
      setItem: jest.fn(),
      getItem: jest.fn(() => null),
    };
    const middleware = createPreserveStateMiddleware(storageMock);
    const storeMock = {
      getState: jest.fn(() => {
        return {
          events: {
            filters: {
              placeId: 1,
            },
          },
        };
      }),
    };
    const nextMock = jest.fn();
    const action = { type: 'SET_EVENTS_PLACE_ID_FILTER' };

    middleware(storeMock)(nextMock)(action);

    expect(storageMock.setItem.mock.calls.length).toEqual(1);
    expect(storageMock.setItem.mock.calls[0][0]).toEqual(config.stateInStorageKey);
    expect(storageMock.setItem.mock.calls[0][1]).toEqual(
      JSON.stringify({
        events: {
          filters: {
            placeId: 1,
          },
        },
      }),
    );
  });

  test.each([
    'SET_RESERVATION_STEP',
    'SET_RESERVATION_PARAMETERS',
    'UPDATE_AGREEMENT',
    'UPDATE_CLIENT_DATA',
    'ADD_OR_REMOVE_SEAT',
    'CHANGE_TICKET_QUANTITY',
    'RESERVATION_CREATED',
  ])('[createPreserveStateMiddleware] should save reservation', actionType => {
    const storageMock = {
      setItem: jest.fn(),
      getItem: jest.fn(() => null),
    };
    const reservation = {
      id: 1,
      eventId: 2,
      showId: 1,
      place: {
        id: 1,
      },
      date: '2019-09-01',
      startTime: '17:00',
      tickets: [
        {
          id: 1,
          quantity: 2,
        },
      ],
      seats: [
        {
          row: 1,
          column: 1,
        },
        {
          row: 1,
          column: 2,
        },
      ],
      takenSeats: [
        {
          row: 1,
          column: 3,
        },
      ],
      agreements: {
        dataProcessing: true,
      },
      clientData: {
        name: 'test',
        surname: 'test',
        email: 'test@test.pl',
        phone: '123 123 123',
      },
      lastVisitedStep: 'seats',
      fromStorage: false,
    };
    const middleware = createPreserveStateMiddleware(storageMock);
    const storeMock = {
      getState: jest.fn(() => {
        return { reservation };
      }),
    };
    const nextMock = jest.fn();
    const action = { type: actionType };

    middleware(storeMock)(nextMock)(action);

    expect(storageMock.setItem.mock.calls.length).toEqual(1);
    expect(storageMock.setItem.mock.calls[0][0]).toEqual(config.stateInStorageKey);
    expect(storageMock.setItem.mock.calls[0][1]).toEqual(
      JSON.stringify({
        reservation,
      }),
    );
  });

  it('[createPreserveStateMiddleware] should remove reservation from storage', () => {
    const storageMock = {
      setItem: jest.fn(),
      getItem: jest.fn(() => {
        return JSON.stringify({
          test: 'test',
          reservation: {
            id: 1,
          },
        });
      }),
    };
    const middleware = createPreserveStateMiddleware(storageMock);
    const storeMock = { getState: jest.fn() };
    const nextMock = jest.fn();
    const action = { type: 'CLEAR_RESERVATION' };

    middleware(storeMock)(nextMock)(action);

    expect(storageMock.setItem.mock.calls.length).toEqual(1);
    expect(storageMock.setItem.mock.calls[0][0]).toEqual(config.stateInStorageKey);
    expect(storageMock.setItem.mock.calls[0][1]).toEqual(JSON.stringify({ test: 'test' }));
  });
});
