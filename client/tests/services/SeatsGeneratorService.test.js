import SeatsGeneratorService from '../../src/services/SeatsGeneratorService';
import Seat from '../../src/models/Seat';

describe('describe SeatsGeneratorService', () => {
  let service, seatsConfiguration, reservation, options, output;

  beforeEach(() => {
    seatsConfiguration = {
      columns: 8,
      rows: 8,
    };
    reservation = {
      seats: [],
      takenSeats: [],
    };
    options = {
      maxWidth: 500,
      maxHeight: 500,
    };
    service = new SeatsGeneratorService(seatsConfiguration, reservation, options);
    output = service.generateSeats();
  });

  it('should return correct Seat object', () => {
    expect(output.seats[0] instanceof Seat).toBeTruthy();
  });

  it('should return correct quantity of Seat object', () => {
    expect(output.seats.length).toEqual(64);
  });

  it('should return Seats with correct status', () => {
    reservation = {
      seats: [
        {
          row: 1,
          column: 1,
        },
      ],
      takenSeats: [
        {
          row: 1,
          column: 2,
        },
      ],
    };
    service = new SeatsGeneratorService(seatsConfiguration, reservation, options);
    output = service.generateSeats();

    expect(output.seats[0].status).toEqual(true);
    expect(output.seats[1].status).toEqual(false);
    expect(output.seats[2].status).toEqual(undefined);
  });
});
