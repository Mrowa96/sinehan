import React, { PureComponent } from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

export default class ProtectedRoute extends PureComponent {
  static propTypes = {
    component: PropTypes.func.isRequired,
    redirectTo: PropTypes.string.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
  };

  render() {
    const { component: Component, isAuthenticated, redirectTo, ...rest } = this.props;

    return (
      <Route {...rest} render={props => (isAuthenticated ? <Component {...props} /> : <Redirect to={redirectTo} />)} />
    );
  }
}
