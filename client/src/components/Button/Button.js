import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.scss';

export default class Button extends PureComponent {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string,
    disabled: PropTypes.bool,
    classList: PropTypes.string,
    children: PropTypes.element,
  };

  static defaultProps = {
    text: 'Next',
    disabled: false,
    classList: '',
    children: undefined,
  };

  render() {
    const { text, onClick, disabled, classList, children } = this.props;

    return (
      <button type="button" onClick={onClick} className={`${styles.button} ${classList}`} disabled={disabled}>
        {children || text}
      </button>
    );
  }
}
