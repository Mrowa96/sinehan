import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './NewsList.module.scss';
import NewsItem from './NewsItem/NewsItem';
import routes from '../../routes';

export default class NewsList extends PureComponent {
  static propTypes = {
    news: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        photo: PropTypes.string.isRequired,
        category: PropTypes.shape({
          name: PropTypes.string.isRequired,
        }),
        content: PropTypes.string.isRequired,
        author: PropTypes.shape({
          name: PropTypes.string.isRequired,
        }),
      }).isRequired,
    ).isRequired,
    linkableTitle: PropTypes.bool,
    showSeeAllButton: PropTypes.bool,
  };

  static defaultProps = {
    linkableTitle: false,
    showSeeAllButton: false,
  };

  render() {
    const { news, showSeeAllButton, linkableTitle } = this.props;
    let content;

    if (news.length) {
      content = (
        <ul className={styles.newsItems}>
          {news.map(newsItem => (
            <NewsItem key={`news_${newsItem.id}`} newsItem={newsItem} />
          ))}
        </ul>
      );
    } else {
      content = <p>No news to display</p>;
    }

    return (
      <section className={styles.newsList}>
        <h1 className={styles.title}>
          {linkableTitle && <Link to={routes.newsList.path}>News</Link>}
          {!linkableTitle && <span>News</span>}

          {showSeeAllButton && news.length > 0 && (
            <Link to={routes.newsList.path} className={styles.seeAllLink}>
              See all
            </Link>
          )}
        </h1>

        {content}
      </section>
    );
  }
}
