import React from 'react';
import Pagination from 'react-js-pagination';
import styles from './Pagination.module.scss';

export default function(props) {
  return (
    <div className={styles.paginationWrapper}>
      <Pagination {...props} disabledClass={styles.disabled} activeClass={styles.active} />
    </div>
  );
}
