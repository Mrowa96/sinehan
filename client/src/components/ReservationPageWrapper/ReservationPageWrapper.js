import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import styles from './ReservationPageWrapper.module.scss';
import QuickSummaryContainer from '../../containers/Reservation/QuickSummaryContainer';
import Button from '../Button/Button';

class ReservationPageWrapper extends Component {
  static propTypes = {
    WrappedComponent: PropTypes.func.isRequired,
  };

  get _summaryClassName() {
    const { quickSummaryShowed } = this.state;

    if (typeof quickSummaryShowed === 'undefined') {
      return '';
    }

    return quickSummaryShowed ? styles.showed : styles.hidden;
  }

  constructor(props) {
    super(props);

    this.state = {
      quickSummaryShowed: undefined,
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize = () => {
    const { quickSummaryShowed } = this.state;

    if (quickSummaryShowed === false) {
      this.setState({ quickSummaryShowed: undefined });
    }
  };

  onAsideButtonClick = () => {
    const { quickSummaryShowed } = this.state;

    this.setState({ quickSummaryShowed: !quickSummaryShowed });
  };

  render() {
    const { WrappedComponent } = this.props;

    return (
      <div className={styles.reservationWrapper}>
        <section className={styles.content}>
          <WrappedComponent />
        </section>

        <aside className={`${styles.summary} ${this._summaryClassName}`}>
          <QuickSummaryContainer />
          <Button type="button" onClick={this.onAsideButtonClick} classList={styles.summaryButton}>
            <span className={styles.angle} />
          </Button>
        </aside>
      </div>
    );
  }
}

export default withRouter(ReservationPageWrapper);
