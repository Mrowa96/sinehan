import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import styles from './NewsDetail.module.scss';

export default class NewsDetail extends Component {
  static propTypes = {
    news: PropTypes.oneOfType([
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        photo: PropTypes.string.isRequired,
        category: PropTypes.shape({
          name: PropTypes.string.isRequired,
        }),
        content: PropTypes.string.isRequired,
        author: PropTypes.shape({
          name: PropTypes.string.isRequired,
        }),
      }),
      PropTypes.exact({}),
    ]),
    updateMeta: PropTypes.bool,
  };

  static defaultProps = {
    news: undefined,
    updateMeta: true,
  };

  get _content() {
    const { news } = this.props;

    return {
      __html: news.content,
    };
  }

  render() {
    const { news, updateMeta } = this.props;
    let content = <p className={styles.notFoundMessage}>News with given id does not exists.</p>;

    if (news && news.id) {
      content = (
        <>
          {updateMeta && (
            <Helmet>
              <title>{`Sinehan - news - ${news.title}`}</title>
            </Helmet>
          )}
          <div className={styles.newsDetail}>
            <h1 className="title">{news.title}</h1>

            <div className={styles.photoAndCategoryWrapper}>
              <img src={news.photo} alt={news.title} />
              {news.category && (
                <div className={styles.categoryWrapper}>
                  <span className={styles.category}>{news.category.name}</span>
                </div>
              )}
            </div>

            <div className={styles.content} dangerouslySetInnerHTML={this._content} />

            {news.author && <i className={styles.author}>{`Author: ${news.author.name}`}</i>}
          </div>
        </>
      );
    }

    return content;
  }
}
