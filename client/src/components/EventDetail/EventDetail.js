import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import styles from './EventDetail.module.scss';
import EventDetailsAndDescription from './EventDetailsAndDescription/EventDetailsAndDescription';
import EventReservations from './EventReservations/EventReservations';

export default class EventDetail extends PureComponent {
  static propTypes = {
    event: PropTypes.oneOfType([
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        trailerUrl: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        shows: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number.isRequired,
            place: PropTypes.shape({
              id: PropTypes.number.isRequired,
              name: PropTypes.string.isRequired,
              room: PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired,
                seats: PropTypes.shape({
                  rows: PropTypes.number.isRequired,
                  columns: PropTypes.number.isRequired,
                }).isRequired,
              }).isRequired,
            }).isRequired,
            date: PropTypes.string.isRequired,
            startTimes: PropTypes.arrayOf(PropTypes.string).isRequired,
          }),
        ).isRequired,
      }),
      PropTypes.exact({}),
    ]),
    updateMeta: PropTypes.bool,
  };

  static defaultProps = {
    event: undefined,
    updateMeta: true,
  };

  render() {
    const { event, updateMeta } = this.props;
    let content = <p className={styles.notFoundMessage}>Event with given id does not exists.</p>;

    if (event && event.id) {
      content = (
        <>
          {updateMeta && event && (
            <Helmet>
              <title>{`Sinehan - event - ${event.title}`}</title>
            </Helmet>
          )}

          <div className={styles.eventDetail}>
            <h1 className="title">{event.title}</h1>

            <div className={styles.trailerWrapper}>
              <iframe
                width="420"
                height="315"
                src={`${event.trailerUrl}?hl=en&iv_load_policy=3&modestbranding=1`}
                frameBorder="0"
                title="trailer"
                allow="autoplay; encrypted-media"
                allowFullScreen
              />
            </div>

            <EventDetailsAndDescription event={event} />

            <EventReservations event={event} />
          </div>
        </>
      );
    }

    return <div className="event-detail">{content}</div>;
  }
}
