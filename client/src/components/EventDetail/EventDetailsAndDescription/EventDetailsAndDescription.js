import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from './EventDetailsAndDescription.module.scss';

export default class EventDetailsAndDescription extends PureComponent {
  static propTypes = {
    event: PropTypes.shape({
      description: PropTypes.string.isRequired,
      originalTitle: PropTypes.string,
      directorName: PropTypes.string,
      production: PropTypes.shape({
        countries: PropTypes.arrayOf(PropTypes.string).isRequired,
        year: PropTypes.number.isRequired,
      }),
      ageLimit: PropTypes.number,
      releaseDate: PropTypes.string,
      duration: PropTypes.number,
    }),
  };

  static defaultProps = {
    event: undefined,
  };

  render() {
    const { event } = this.props;

    return (
      <div className={styles.detailsAndDescription}>
        <div className={styles.descriptionWrapper}>
          <h3 className="subtitle">Description</h3>
          <p>{event.description}</p>
        </div>

        <div className={styles.detailsWrapper}>
          <h3 className="subtitle">Details</h3>

          <table>
            <tbody>
              {event.originalTitle && (
                <tr>
                  <td>Original title:</td>
                  <td>{event.originalTitle}</td>
                </tr>
              )}
              {event.directorName && (
                <tr>
                  <td>Director:</td>
                  <td>{event.directorName}</td>
                </tr>
              )}
              {event.production && (
                <tr className={styles.production}>
                  <td>Production:</td>
                  <td>
                    <span className={styles.countries}>
                      {event.production.countries.map(country => (
                        <span className={styles.country} key={`country_${country}`}>
                          {country}
                        </span>
                      ))}
                    </span>
                    <span className={styles.year}>{event.production.year}</span>
                  </td>
                </tr>
              )}
              {event.ageLimit && (
                <tr>
                  <td>Age limit:</td>
                  <td>{event.ageLimit}</td>
                </tr>
              )}
              {event.releaseDate && (
                <tr>
                  <td>Release date:</td>
                  <td>{event.releaseDate}</td>
                </tr>
              )}
              {event.duration && (
                <tr>
                  <td>Duration:</td>
                  <td>{`${event.duration} minutes`}</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
