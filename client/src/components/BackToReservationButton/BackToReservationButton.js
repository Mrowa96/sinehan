import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Button from '../Button/Button';
import styles from './BackToReservationButton.module.scss';

export class BackToReservationButton extends Component {
  static propTypes = {
    route: PropTypes.shape({
      path: PropTypes.string.isRequired,
    }).isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  onClick = () => {
    const { history, route } = this.props;

    history.push(route.path);
  };

  render() {
    return <Button type="button" classList={styles.reservationButton} onClick={this.onClick} text="Reservation" />;
  }
}

export default withRouter(BackToReservationButton);
