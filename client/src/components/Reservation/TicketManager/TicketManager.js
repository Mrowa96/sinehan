import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from './TicketManager.module.scss';
import Button from '../../Button/Button';

export default class TicketManager extends PureComponent {
  static propTypes = {
    tickets: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
      }).isRequired,
    ),
    selectedTickets: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        quantity: PropTypes.number.isRequired,
      }).isRequired,
    ).isRequired,
    maxTicketQuantity: PropTypes.number,
    onChange: PropTypes.func.isRequired,
    onNextButtonClick: PropTypes.func.isRequired,
    onPrevButtonClick: PropTypes.func.isRequired,
  };

  static defaultProps = {
    tickets: [],
    maxTicketQuantity: 10,
  };

  get _buttonShouldBeDisabled() {
    const { selectedTickets } = this.props;

    return !selectedTickets.length;
  }

  onChange = (ticketId, event) => {
    const { onChange } = this.props;

    onChange(+ticketId, +event.target.value);
  };

  getDefaultValueForTicketId(ticketId) {
    const { selectedTickets } = this.props;
    const selectedTicket = selectedTickets.find(ticket => ticket.id === ticketId);

    return selectedTicket ? selectedTicket.quantity : 0;
  }

  getMaxTicketQuantity = ticketId => {
    const { selectedTickets, maxTicketQuantity } = this.props;
    const selectedTicket = selectedTickets.find(ticket => ticket.id === ticketId);
    const allSelectedTicketsQuantity = selectedTickets.reduce((total, { quantity }) => {
      total += quantity;

      return total;
    }, 0);

    return maxTicketQuantity - allSelectedTicketsQuantity + (selectedTicket ? selectedTicket.quantity : 0);
  };

  render() {
    const { tickets, onNextButtonClick, onPrevButtonClick } = this.props;

    return (
      <div className={styles.ticketsManager}>
        <h1 className="title">Select tickets</h1>

        {!!tickets.length && (
          <ul className={styles.tickets}>
            {tickets.map(ticket => (
              <li key={`ticket_${ticket.id}`} className={styles.ticket}>
                <span className={styles.details}>
                  <span className={styles.name}>
                    {ticket.name}
                    {' ticket'}
                  </span>
                  <span className={styles.price}>
                    {ticket.price}
                    {' PLN'}
                  </span>
                </span>

                <select
                  disabled={this.getMaxTicketQuantity(ticket.id) === 1}
                  defaultValue={this.getDefaultValueForTicketId(ticket.id)}
                  onChange={event => {
                    this.onChange(ticket.id, event);
                  }}
                >
                  {Array.from(Array(this.getMaxTicketQuantity(ticket.id))).map((value, index) => (
                    <option key={`ticket_${ticket.id}_quantity_${index}`} value={index}>
                      {index}
                    </option>
                  ))}
                </select>
              </li>
            ))}
          </ul>
        )}

        {!tickets.length && <p className={styles.noTickets}>No tickets found</p>}

        <div className={styles.buttons}>
          <Button type="button" onClick={onPrevButtonClick} text="Prev" />
          <Button type="button" onClick={onNextButtonClick} disabled={this._buttonShouldBeDisabled} />
        </div>
      </div>
    );
  }
}
