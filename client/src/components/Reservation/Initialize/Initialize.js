import React from 'react';
import Loader from '../../Loader/Loader';
import styles from './Initialize.module.scss';

export default function Initialize() {
  return (
    <div className={styles.wrapper}>
      <Loader loading inline />
      <span className={styles.message}>We are creating your reservation...</span>
    </div>
  );
}
