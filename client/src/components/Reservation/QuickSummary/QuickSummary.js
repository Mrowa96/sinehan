import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './QuickSummary.module.scss';

export default class QuickSummary extends PureComponent {
  static propTypes = {
    event: PropTypes.shape({
      title: PropTypes.string.isRequired,
      posterUrl: PropTypes.string.isRequired,
      duration: PropTypes.number,
    }).isRequired,
    place: PropTypes.shape({
      name: PropTypes.string.isRequired,
    }).isRequired,
    tickets: PropTypes.objectOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
      }),
    ),
    reservation: PropTypes.shape({
      date: PropTypes.string.isRequired,
      startTime: PropTypes.string.isRequired,
      tickets: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number.isRequired,
          quantity: PropTypes.number.isRequired,
        }),
      ).isRequired,
      seats: PropTypes.arrayOf(
        PropTypes.shape({
          row: PropTypes.number.isRequired,
          column: PropTypes.number.isRequired,
        }),
      ).isRequired,
    }).isRequired,
    seats: PropTypes.arrayOf(
      PropTypes.shape({
        row: PropTypes.number.isRequired,
        columns: PropTypes.arrayOf(PropTypes.number).isRequired,
      }),
    ).isRequired,
    totalCost: PropTypes.number.isRequired,
  };

  static defaultProps = {
    tickets: undefined,
  };

  render() {
    const { event, reservation, place, tickets, totalCost, seats } = this.props;

    return (
      <div className={styles.summary}>
        <h1 className="title">Quick summary</h1>

        <div className={styles.summaryContent}>
          <Link to={`/event-detail/${event.id}`} className={styles.posterWrapper}>
            <img src={event.posterUrl} alt={event.title} />
          </Link>

          <div className={styles.details}>
            <Link to={`/event-detail/${event.id}`} className={styles.eventTitle}>
              {event.title}
            </Link>

            <table>
              <tbody>
                <tr>
                  <td>Place:</td>
                  <td>{place.name}</td>
                </tr>
                <tr>
                  <td>Date:</td>
                  <td>{reservation.date}</td>
                </tr>
                <tr>
                  <td>Start time:</td>
                  <td>{reservation.startTime}</td>
                </tr>
                {!!event.duration && (
                  <tr>
                    <td>Duration:</td>
                    <td>
                      {event.duration}
                      {' minutes'}
                    </td>
                  </tr>
                )}
                {!!tickets && (
                  <tr>
                    <td>Tickets:</td>
                    <td>
                      {reservation.tickets.length > 0 &&
                        reservation.tickets.map(ticket => (
                          <span key={`ticket_${ticket.id}`} className={styles.ticket}>
                            {ticket.quantity}
                            {' x'}
                            {tickets[ticket.id].name}
                          </span>
                        ))}
                      {reservation.tickets.length === 0 && <span className={styles.ticket}>None</span>}
                    </td>
                  </tr>
                )}
                <tr>
                  <td>Seats:</td>
                  <td>
                    {seats.length > 0 &&
                      seats.map(seat => (
                        <span className={styles.seat} key={`seats_in_row_${seat.row}`}>
                          <span className={styles.seatRow}>
                            <span className={styles.label}>Row:</span>
                            <span className={styles.rowNumber}>{seat.row}</span>
                          </span>
                          <span className={styles.seatPlaces}>
                            <span className={styles.label}>{seat.columns.length === 1 ? 'Place:' : 'Places:'}</span>
                            <span className={styles.placesNumbers}>
                              {seat.columns.map(column => (
                                <span key={`seat_${seat.row}_${column}`} className={styles.placeNumber}>
                                  {column}
                                </span>
                              ))}
                            </span>
                          </span>
                        </span>
                      ))}
                    {seats.length === 0 && <span className={styles.seat}>None</span>}
                  </td>
                </tr>
                <tr>
                  <td>Cost:</td>
                  <td>
                    {totalCost}
                    {' PLN'}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
