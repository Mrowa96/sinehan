import React from 'react';
import Loader from '../../Loader/Loader';
import styles from './Restore.module.scss';

export default function Restore() {
  return (
    <div className={styles.wrapper}>
      <Loader loading inline />
      <span className={styles.message}>We are restoring your reservation...</span>
    </div>
  );
}
