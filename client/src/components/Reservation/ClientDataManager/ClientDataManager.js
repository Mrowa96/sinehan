import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './ClientDataManager.module.scss';
import Button from '../../Button/Button';

export default class ClientDataManager extends Component {
  static propTypes = {
    reservation: PropTypes.shape({
      clientData: PropTypes.object.isRequired,
      agreements: PropTypes.shape({
        dataProcessing: PropTypes.bool,
      }).isRequired,
    }).isRequired,
    onClientDataChange: PropTypes.func.isRequired,
    onAgreementChange: PropTypes.func.isRequired,
    triggerClientDataSave: PropTypes.func.isRequired,
    onNextButtonClick: PropTypes.func.isRequired,
    onPrevButtonClick: PropTypes.func.isRequired,
  };

  get _clientData() {
    const { reservation } = this.props;

    return reservation.clientData;
  }

  get _agreements() {
    const { reservation } = this.props;

    return reservation.agreements;
  }

  constructor(props) {
    super(props);

    this.formRef = React.createRef();
  }

  onNextButtonClick = () => {
    const { onNextButtonClick } = this.props;
    const formNode = this.formRef.current;

    if (formNode) {
      if (formNode.checkValidity()) {
        onNextButtonClick();
      } else {
        formNode.reportValidity();
      }
    }
  };

  onInputChange = (property, event) => {
    const { onClientDataChange } = this.props;
    const { value } = event.target;

    onClientDataChange(property, value);
  };

  onInputBlur = () => {
    const { reservation, triggerClientDataSave } = this.props;

    if (reservation.agreements.dataProcessing) {
      triggerClientDataSave();
    }
  };

  onCheckboxChange = (property, event) => {
    const { onAgreementChange } = this.props;

    onAgreementChange(property, event.target.checked);
  };

  render() {
    const { onPrevButtonClick } = this.props;

    return (
      <div className={styles.clientDataManager}>
        <h1 className="title">Give us some personal data</h1>

        <form className={styles.clientForm} ref={this.formRef}>
          <div className={styles.formControl}>
            <label htmlFor="client-name">Name:</label>
            <input
              type="text"
              id="client-name"
              name="name"
              maxLength="128"
              value={this._clientData.name}
              onChange={event => {
                this.onInputChange('name', event);
              }}
              onBlur={this.onInputBlur}
              required
            />
          </div>

          <div className={styles.formControl}>
            <label htmlFor="client-surname">Surname:</label>
            <input
              type="text"
              id="client-surname"
              name="surname"
              maxLength="128"
              value={this._clientData.surname}
              onChange={event => {
                this.onInputChange('surname', event);
              }}
              onBlur={this.onInputBlur}
              required
            />
          </div>

          <div className={styles.formControl}>
            <label htmlFor="client-email">Email:</label>
            <input
              type="email"
              id="client-name"
              name="email"
              maxLength="256"
              value={this._clientData.email}
              onChange={event => {
                this.onInputChange('email', event);
              }}
              onBlur={this.onInputBlur}
              required
            />
          </div>

          <div className={styles.formControl}>
            <label htmlFor="client-phone">Phone:</label>
            <input
              type="tel"
              id="client-phone"
              name="phone"
              maxLength="32"
              pattern="[0-9]{3} [0-9]{3} [0-9]{3}"
              placeholder="123 456 789"
              value={this._clientData.phone}
              onChange={event => {
                this.onInputChange('phone', event);
              }}
              onBlur={this.onInputBlur}
              required
            />
          </div>

          <div className={styles.formCheckboxControl}>
            <input
              type="checkbox"
              id="data-processing-agreement"
              name="data-processing-agreement"
              checked={this._agreements.dataProcessing}
              onChange={event => {
                this.onCheckboxChange('dataProcessing', event);
              }}
              required
            />
            <label htmlFor="data-processing-agreement">Yeah, I accept everything what you want from me.</label>
          </div>
        </form>

        <div className={styles.buttons}>
          <Button type="button" onClick={onPrevButtonClick} text="Prev" />
          <Button type="button" onClick={this.onNextButtonClick} text="Make a reservation" />
        </div>
      </div>
    );
  }
}
