import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './EventPlaceFilter.module.scss';
import { nearestPlaceId as getNearestPlaceId } from '../../../utils/nearestPlace';

export default class EventPlaceFilter extends Component {
  static propTypes = {
    places: PropTypes.objectOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
      }),
    ),
    onChange: PropTypes.func.isRequired,
    selectedPlaceId: PropTypes.number,
    locked: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    places: undefined,
    selectedPlaceId: undefined,
  };

  get _options() {
    return [...this._placesAsArray.map(place => ({ value: place.id, label: place.name }))];
  }

  get _placesAsArray() {
    const { places } = this.props;

    return Object.values(places);
  }

  get _disableGeolocationButton() {
    return !navigator.geolocation && !this._placesAsArray.length;
  }

  constructor(props) {
    super(props);

    this.state = {
      selectedPlaceId: props.places[props.selectedPlaceId]
        ? this._generateSelectedPlaceIdState(props.selectedPlaceId)
        : null,
    };
  }

  componentDidUpdate(prevProps) {
    const { places, selectedPlaceId } = this.props;

    if (selectedPlaceId !== prevProps.selectedPlaceId && places[selectedPlaceId]) {
      this.setState({ selectedPlaceId: this._generateSelectedPlaceIdState(selectedPlaceId) });
    }
  }

  onGeolocationButtonClick = () => {
    const { onChange } = this.props;

    if (navigator && navigator.geolocation && this._placesAsArray.length) {
      navigator.geolocation.getCurrentPosition(position => {
        const nearestPlaceId = getNearestPlaceId(position.coords, this._placesAsArray);

        if (nearestPlaceId) {
          onChange(nearestPlaceId);
        }
      });
    }
  };

  onSelectChange = selectedPlaceId => {
    const value = selectedPlaceId.value ? +selectedPlaceId.value : null;
    const { onChange } = this.props;

    onChange(value);
  };

  _generateSelectedPlaceIdState(placeId) {
    const { places } = this.props;

    return {
      value: placeId,
      label: places[placeId].name,
    };
  }

  render() {
    const { selectedPlaceId } = this.state;
    const { locked } = this.props;

    return (
      <div className={`${styles.placeFilter} ${locked ? styles.locked : ''}`}>
        <Select
          className={styles.placesDropdown}
          value={selectedPlaceId}
          onChange={this.onSelectChange}
          options={this._options}
          placeholder="Select place"
        />
        <button
          type="button"
          className={styles.geolocationButton}
          disabled={this._disableGeolocationButton}
          onClick={this.onGeolocationButtonClick}
        >
          <FontAwesomeIcon icon={['fas', 'compass']} />
        </button>
      </div>
    );
  }
}
