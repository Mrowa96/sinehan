import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import styles from './EventDateFilter.module.scss';

export class EventDateFilter extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    selectedDate: PropTypes.string,
    locked: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    selectedDate: undefined,
  };

  get _nextDates() {
    const dates = [];
    const currentDate = moment();
    let label;
    let date;
    let index = 0;

    while (index < 8) {
      date = currentDate.format('YYYY-MM-DD');

      if (index === 0) {
        label = 'Today';
      } else if (index === 1) {
        label = 'Tomorrow';
      } else {
        label = date;
      }

      dates.push({
        value: date,
        label,
        active: this._selectedDate.format('YYYY-MM-DD') === date,
      });

      currentDate.add(1, 'days');

      index += 1;
    }

    return dates;
  }

  get _selectedDate() {
    const { selectedDate } = this.props;

    return moment(selectedDate || undefined);
  }

  constructor(props) {
    super(props);

    this.state = {
      minDate: moment(),
      maxDate: moment().add(14, 'days'),
    };
  }

  onDataPickerChange = date => {
    const { onChange } = this.props;
    const value = date ? date.format('YYYY-MM-DD') : null;

    onChange(value);
  };

  onConcreteDateButtonClick = date => {
    const { onChange } = this.props;

    onChange(date);
  };

  render() {
    const { locked } = this.props;
    const { minDate, maxDate } = this.state;

    return (
      <ul className={styles.dateList}>
        {this._nextDates.map(date => (
          <li
            key={`date_{${date.value}`}
            className={`${date.active ? styles.active : ''} ${locked ? styles.locked : ''}`}
          >
            <button
              type="button"
              className={styles.dateButton}
              onClick={() => {
                this.onConcreteDateButtonClick(date.value);
              }}
            >
              {date.label}
            </button>
          </li>
        ))}
        <li className={`${styles.datePickerWrapper} ${locked ? styles.locked : ''}`}>
          <DatePicker
            className={styles.datePickerInput}
            selected={this._selectedDate}
            onChange={this.onDataPickerChange}
            dropdownMode="select"
            minDate={minDate}
            maxDate={maxDate}
          />
        </li>
      </ul>
    );
  }
}
