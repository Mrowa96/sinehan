import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import EventPlaceFilterContainer from '../../containers/FilteredEventList/EventPlaceFilterContainer';
import EventDateFilterContainer from '../../containers/FilteredEventList/EventDateFilterContainer';
import styles from './FilteredEventList.module.scss';
import EventItemToReserveContainer from '../../containers/FilteredEventList/EventItemToReserveContainer';
import Loader from '../Loader/Loader';

export default class FilteredEventList extends PureComponent {
  static propTypes = {
    events: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        posterUrl: PropTypes.string.isRequired,
        shows: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number.isRequired,
            place: PropTypes.shape({
              id: PropTypes.number.isRequired,
              name: PropTypes.string.isRequired,
              room: PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired,
                seats: PropTypes.shape({
                  rows: PropTypes.number.isRequired,
                  columns: PropTypes.number.isRequired,
                }).isRequired,
              }).isRequired,
            }).isRequired,
            date: PropTypes.string.isRequired,
            startTimes: PropTypes.arrayOf(PropTypes.string).isRequired,
          }),
        ).isRequired,
      }),
    ),
    eventsLoading: PropTypes.bool.isRequired,
    filters: PropTypes.shape({
      placeId: PropTypes.number,
      date: PropTypes.string,
    }),
  };

  static defaultProps = {
    events: [],
    filters: undefined,
  };

  render() {
    let content;
    const { events, eventsLoading, filters } = this.props;

    if (eventsLoading) {
      content = <Loader loading={eventsLoading} />;
    } else if (events && events.length) {
      content = (
        <ul className={styles.list}>
          {events.map(event => (
            <EventItemToReserveContainer key={`event_${event.id}`} event={event} />
          ))}
        </ul>
      );
    } else if (!filters || !filters.placeId) {
      content = <p>Please select place to display events.</p>;
    } else {
      content = <p>No events to display.</p>;
    }

    return (
      <section className="event-list">
        <h1 className="title">Make a reservation</h1>

        <div className={styles.filters}>
          <div className={styles.placeFilter}>
            <EventPlaceFilterContainer locked={eventsLoading} />
          </div>

          <div className={styles.dateFilter}>
            <EventDateFilterContainer locked={eventsLoading} />
          </div>
        </div>

        {content}
      </section>
    );
  }
}
