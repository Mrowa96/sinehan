import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from './Header/Header';
import Navigation from './Navigation/Navigation';
import styles from './Layout.module.scss';
import BackToReservationButtonContainer from '../../containers/BackToReservationButtonContainer';

export default class Layout extends Component {
  static propTypes = {
    children: PropTypes.element,
  };

  static defaultProps = {
    children: undefined,
  };

  #topBarRef = null;

  #additionalTopOffset = 20;

  #defaultOffset = 93.75;

  get _contentOffsetTop() {
    const { topBarHeight } = this.state;

    return this.#topBarRef.current
      ? topBarHeight + this.#additionalTopOffset
      : this.#defaultOffset + this.#additionalTopOffset;
  }

  constructor(props) {
    super(props);

    this.#topBarRef = React.createRef();

    this.state = {
      topBarHeight: 0,
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateHeight);

    this.updateHeight();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateHeight);
  }

  updateHeight = () => {
    const { topBarHeight } = this.state;
    const topBarNode = this.#topBarRef.current;

    if (topBarNode && topBarNode.offsetHeight !== topBarHeight) {
      this.setState({ topBarHeight: topBarNode.offsetHeight });
    }
  };

  render() {
    const { children } = this.props;

    return (
      <>
        <div className={styles.topBar} ref={this.#topBarRef}>
          <Header />
          <Navigation />
        </div>

        <main className={styles.content} style={{ marginTop: this._contentOffsetTop }}>
          {children}
        </main>

        <BackToReservationButtonContainer />
      </>
    );
  }
}
