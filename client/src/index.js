import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { BrowserRouter } from 'react-router-dom';
import createSagaMiddleware from 'redux-saga';
import App from './components/App/App';
import * as serviceWorker from './serviceWorker';
import reducers from './state/reducers';
import { initialState as defaultState } from './state/store/initialState';
import sagas from './state/sagas';
import { createPreserveStateMiddleware } from './state/middlewares/preserveStateMiddleware';
import { setEventsPlaceIdFilter } from './state/actions/events';
import { restoreReservation } from './state/actions/reservation';
import isValid from './utils/validateReservationFromStorage';
import config from './config';

let initialState;

if (window && window.__INITIAL_DATA__) {
  initialState = window.__INITIAL_DATA__;

  delete window.__INITIAL_DATA__;

  const initialDataElement = document.getElementById('initialData');

  if (initialDataElement) {
    initialDataElement.parentNode.removeChild(initialDataElement);
  }
} else {
  initialState = defaultState;
}

const sagaMiddleware = createSagaMiddleware();
const preserveStateMiddleware = createPreserveStateMiddleware();
const store = createStore(reducers, initialState, applyMiddleware(sagaMiddleware, preserveStateMiddleware));

sagaMiddleware.run(sagas);

ReactDOM.hydrate(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('app'),
);

// TODO Test it somehow?
const stateFromStorage = JSON.parse(localStorage.getItem(config.stateInStorageKey));

if (stateFromStorage) {
  if (stateFromStorage.reservation) {
    if (isValid(stateFromStorage.reservation)) {
      store.dispatch(restoreReservation(stateFromStorage.reservation));
    } else {
      delete stateFromStorage.reservation;

      localStorage.setItem(config.stateInStorageKey, JSON.stringify(stateFromStorage));
    }
  }

  if (stateFromStorage.events && stateFromStorage.events.filters && stateFromStorage.events.filters.placeId) {
    store.dispatch(setEventsPlaceIdFilter(stateFromStorage.events.filters.placeId));
  }
}

if (process.env.NODE_ENV === 'development') {
  store.subscribe(() => {
    console.log(store.getState());
  });
}

serviceWorker.unregister();
