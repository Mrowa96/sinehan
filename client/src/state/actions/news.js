export const LOAD_ONE_NEWS = 'LOAD_ONE_NEWS';
export const LOAD_NEWS = 'LOAD_NEWS';
export const ONE_NEWS_FETCHED = 'ONE_NEWS_FETCHED';
export const NEWS_NOT_FOUND = 'NEWS_NOT_FOUND';
export const FILTERED_NEWS_FETCHED = 'FILTERED_NEWS_FETCHED';

export function loadNews(requestId, filters) {
  return {
    type: LOAD_NEWS,
    payload: {
      requestId,
      filters,
    },
  };
}

export function loadOneNews(id, requestId) {
  return {
    type: LOAD_ONE_NEWS,
    payload: {
      id,
      requestId,
    },
  };
}

export function oneNewsFetched(response, request) {
  return {
    type: ONE_NEWS_FETCHED,
    payload: {
      response,
      request,
    },
  };
}

export function newsNotFound(request) {
  return {
    type: NEWS_NOT_FOUND,
    payload: {
      request,
    },
  };
}

export function filteredNewsFetched(response, request) {
  return {
    type: FILTERED_NEWS_FETCHED,
    payload: {
      response,
      request,
    },
  };
}
