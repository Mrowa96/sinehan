export const CHANGE_TICKET_QUANTITY = 'CHANGE_TICKET_QUANTITY';
export const SET_RESERVATION_PARAMETERS = 'SET_RESERVATION_PARAMETERS';
export const CLEAR_RESERVATION = 'CLEAR_RESERVATION';
export const ADD_OR_REMOVE_SEAT = 'ADD_OR_REMOVE_SEAT';
export const UPDATE_CLIENT_DATA = 'UPDATE_CLIENT_DATA';
export const SET_RESERVATION_STEP = 'SET_RESERVATION_STEP';
export const CREATE_RESERVATION = 'CREATE_RESERVATION';
export const RESERVATION_CREATED = 'RESERVATION_CREATED';
export const SAVE_RESERVATION = 'SAVE_RESERVATION';
export const UPDATE_AGREEMENT = 'UPDATE_AGREEMENT';
export const DELETE_RESERVATION = 'DELETE_RESERVATION';
export const COMPLETE_RESERVATION = 'COMPLETE_RESERVATION';
export const RESTORE_RESERVATION = 'RESTORE_RESERVATION';

export function createReservation(showId, startTime, requestId) {
  return {
    type: CREATE_RESERVATION,
    payload: {
      showId,
      startTime,
      requestId,
    },
  };
}

export function reservationCreated(response, request) {
  return {
    type: RESERVATION_CREATED,
    payload: {
      response,
      request,
    },
  };
}

export function saveReservation(properties, requestId = Symbol('saveReservation')) {
  return {
    type: SAVE_RESERVATION,
    payload: {
      properties,
      requestId,
    },
  };
}

export function deleteReservation(reservationId, requestId) {
  return {
    type: DELETE_RESERVATION,
    payload: {
      reservationId,
      requestId,
    },
  };
}

export function completeReservation(reservationId, requestId) {
  return {
    type: COMPLETE_RESERVATION,
    payload: {
      reservationId,
      requestId,
    },
  };
}

export function changeTicketQuantity(ticketId, quantity) {
  return {
    type: CHANGE_TICKET_QUANTITY,
    payload: {
      ticketId,
      quantity,
    },
  };
}

export function addOrRemoveSeat(column, row) {
  return {
    type: ADD_OR_REMOVE_SEAT,
    payload: {
      column,
      row,
    },
  };
}

export function updateClientData(property, value) {
  return {
    type: UPDATE_CLIENT_DATA,
    payload: {
      property,
      value,
    },
  };
}

export function updateAgreement(agreement, value) {
  return {
    type: UPDATE_AGREEMENT,
    payload: {
      agreement,
      value,
    },
  };
}

export function setReservationParameters(parameters) {
  return {
    type: SET_RESERVATION_PARAMETERS,
    payload: {
      parameters,
    },
  };
}

export function clearReservation() {
  return {
    type: CLEAR_RESERVATION,
    payload: {},
  };
}

export function setReservationStep(step) {
  return {
    type: SET_RESERVATION_STEP,
    payload: {
      step,
    },
  };
}

export function restoreReservation(reservation) {
  return {
    type: RESTORE_RESERVATION,
    payload: {
      reservation,
    },
  };
}
