export const LOAD_PLACES = 'LOAD_PLACES';
export const PLACES_FETCHED = 'PLACES_FETCHED';

export function placesFetched(response) {
  return {
    type: PLACES_FETCHED,
    payload: {
      response,
    },
  };
}

export function loadPlaces(requestId) {
  return {
    type: LOAD_PLACES,
    payload: {
      requestId,
    },
  };
}
