import { initialState } from '../store/initialState';
import {
  ADD_OR_REMOVE_SEAT,
  CHANGE_TICKET_QUANTITY,
  CLEAR_RESERVATION,
  RESERVATION_CREATED,
  RESTORE_RESERVATION,
  SET_RESERVATION_PARAMETERS,
  SET_RESERVATION_STEP,
  UPDATE_AGREEMENT,
  UPDATE_CLIENT_DATA,
} from '../actions/reservation';

export function reservation(state = initialState.reservation, action) {
  let tickets;
  let existingTicketIndex;
  let seats;
  let existingSeatIndex;
  let ticketsMaxQuantity;

  switch (action.type) {
    case RESERVATION_CREATED:
      return {
        ...initialState.reservation,
        id: action.payload.response.data.id,
        eventId: action.payload.response.data.eventId,
        showId: action.payload.response.data.showId,
        place: action.payload.response.data.place,
        date: action.payload.response.data.date,
        startTime: action.payload.response.data.startTime,
        takenSeats: action.payload.response.data.takenSeats,
      };

    case CHANGE_TICKET_QUANTITY:
      existingTicketIndex = state.tickets.findIndex(ticket => ticket.id === action.payload.ticketId);
      tickets = [...state.tickets];

      if (existingTicketIndex !== -1) {
        if (action.payload.quantity < 1) {
          tickets.splice(existingTicketIndex, 1);
        } else {
          tickets[existingTicketIndex].quantity = action.payload.quantity;
        }
      } else {
        tickets.push({ id: action.payload.ticketId, quantity: action.payload.quantity });
      }

      return {
        ...state,
        tickets,
        seats: initialState.reservation.seats,
      };

    case SET_RESERVATION_PARAMETERS:
      return Object.entries(action.payload.parameters).reduce(
        (reservationState, [property, value]) => {
          if (Object.prototype.hasOwnProperty.call(reservationState, property)) {
            reservationState[property] = value;
          }

          return reservationState;
        },
        { ...initialState.reservation },
      );

    case CLEAR_RESERVATION:
      return {
        ...initialState.reservation,
      };

    case ADD_OR_REMOVE_SEAT:
      existingSeatIndex = state.seats.findIndex(
        seat => seat.column === action.payload.column && seat.row === action.payload.row,
      );
      seats = [...state.seats];

      if (existingSeatIndex !== -1) {
        seats.splice(existingSeatIndex, 1);

        return {
          ...state,
          seats,
        };
      }

      ticketsMaxQuantity = state.tickets.reduce((previous, ticket) => previous + ticket.quantity, 0);

      if (state.seats.length < ticketsMaxQuantity) {
        return {
          ...state,
          seats: [...seats, { column: action.payload.column, row: action.payload.row }],
        };
      }
      return state;

    case UPDATE_CLIENT_DATA:
      return {
        ...state,
        clientData: {
          ...state.clientData,
          [action.payload.property]: action.payload.value,
        },
      };

    case UPDATE_AGREEMENT:
      return {
        ...state,
        agreements: {
          ...state.agreements,
          [action.payload.agreement]: action.payload.value,
        },
      };

    case SET_RESERVATION_STEP:
      return {
        ...state,
        lastVisitedStep: action.payload.step,
      };

    case RESTORE_RESERVATION:
      return {
        ...state,
        ...action.payload.reservation,
        fromStorage: true,
      };

    default:
      return state;
  }
}
