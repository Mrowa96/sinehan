import {
  EVENT_FETCHED,
  EVENT_NOT_FOUND,
  FILTERED_EVENTS_FETCHED,
  SET_EVENTS_DATE_FILTER,
  SET_EVENTS_PLACE_ID_FILTER,
} from '../actions/events';
import { initialState } from '../store/initialState';
import { arrayToObject } from '../../utils/arrayToObject';
import { serializeFilters } from '../../utils/serializeFilters';

export function events(state = initialState.events, action) {
  let eventsAsObject;

  switch (action.type) {
    case FILTERED_EVENTS_FETCHED:
      eventsAsObject = arrayToObject(action.payload.response.data);

      return {
        ...state,
        byId: {
          ...state.byId,
          ...eventsAsObject,
        },
        dataByFilter: {
          ...state.dataByFilter,
          [serializeFilters(action.payload.request.filters)]: {
            ids: Object.keys(eventsAsObject).map(id => +id),
            totalQuantity: +action.payload.response.headers['x-total-count'],
          },
        },
      };

    case EVENT_FETCHED:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.response.data.id]: action.payload.response.data,
        },
        lastSeenIds: [...state.lastSeenIds, action.payload.response.data.id],
      };

    case EVENT_NOT_FOUND:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.request.filters.id]: {},
        },
      };

    case SET_EVENTS_PLACE_ID_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          placeId: action.payload.placeId,
        },
      };

    case SET_EVENTS_DATE_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          date: action.payload.date,
        },
      };

    default:
      return state;
  }
}
