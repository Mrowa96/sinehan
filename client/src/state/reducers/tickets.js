import { initialState } from '../store/initialState';
import { arrayToObject } from '../../utils/arrayToObject';
import { TICKETS_FETCHED } from '../actions/tickets';

export function tickets(state = initialState.tickets, action) {
  let ticketsAsObject;

  switch (action.type) {
    case TICKETS_FETCHED:
      ticketsAsObject = arrayToObject(action.payload.response.data);

      return {
        ...state,
        byId: {
          ...state.byId,
          ...ticketsAsObject,
        },
      };

    default:
      return state;
  }
}
