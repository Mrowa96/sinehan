import { SET_EVENTS_PLACE_ID_FILTER } from '../actions/events';
import {
  ADD_OR_REMOVE_SEAT,
  CHANGE_TICKET_QUANTITY,
  CLEAR_RESERVATION,
  RESERVATION_CREATED,
  SET_RESERVATION_PARAMETERS,
  SET_RESERVATION_STEP,
  UPDATE_AGREEMENT,
  UPDATE_CLIENT_DATA,
} from '../actions/reservation';
import config from '../../config';

const allowedReservationActions = [
  SET_RESERVATION_STEP,
  SET_RESERVATION_PARAMETERS,
  UPDATE_AGREEMENT,
  UPDATE_CLIENT_DATA,
  ADD_OR_REMOVE_SEAT,
  CHANGE_TICKET_QUANTITY,
  RESERVATION_CREATED,
];

export function createPreserveStateMiddleware(storage = localStorage) {
  return ({ getState }) => next => action => {
    const stateToPreserve = JSON.parse(storage.getItem(config.stateInStorageKey)) || {};

    next(action);

    const state = getState();

    if (action.type === SET_EVENTS_PLACE_ID_FILTER) {
      stateToPreserve.events = {
        filters: {
          placeId: state.events.filters.placeId,
        },
      };
    }

    if (allowedReservationActions.includes(action.type)) {
      stateToPreserve.reservation = {
        ...state.reservation,
      };
    }

    if (action.type === CLEAR_RESERVATION) {
      delete stateToPreserve.reservation;
    }

    storage.setItem(config.stateInStorageKey, JSON.stringify(stateToPreserve));
  };
}
