import { put } from 'redux-saga/effects';
import { placesFetched } from '../actions/places';
import { requestGet } from '../actions/requests';

export function* loadPlaces(action) {
  yield put(requestGet('place', {}, action.payload.requestId, placesFetched));
}
