import { takeEvery } from 'redux-saga/effects';
import {
  REQUEST_GET,
  REQUEST_POST,
  REQUEST_PUT,
  REQUEST_SUCCEED,
  REQUEST_FAILED,
  REQUEST_DELETE,
} from '../actions/requests';
import { LOAD_EVENT, LOAD_EVENTS } from '../actions/events';
import { LOAD_PLACES } from '../actions/places';
import { LOAD_TICKETS } from '../actions/tickets';
import { LOAD_NEWS, LOAD_ONE_NEWS } from '../actions/news';
import { COMPLETE_RESERVATION, CREATE_RESERVATION, DELETE_RESERVATION, SAVE_RESERVATION } from '../actions/reservation';
import { requestGet, requestPost, requestPut, requestSucceed, requestFailed, requestDelete } from './requests';
import { loadEvent, loadEvents } from './events';
import { loadPlaces } from './places';
import { loadTickets } from './tickets';
import { loadNews, loadOneNews } from './news';
import { completeReservation, createReservation, deleteReservation, saveReservation } from './reservation';

export default function* sagas() {
  yield takeEvery(REQUEST_GET, requestGet);
  yield takeEvery(REQUEST_DELETE, requestDelete);
  yield takeEvery(REQUEST_POST, requestPost);
  yield takeEvery(REQUEST_PUT, requestPut);
  yield takeEvery(REQUEST_SUCCEED, requestSucceed);
  yield takeEvery(REQUEST_FAILED, requestFailed);
  yield takeEvery(LOAD_EVENT, loadEvent);
  yield takeEvery(LOAD_EVENTS, loadEvents);
  yield takeEvery(LOAD_PLACES, loadPlaces);
  yield takeEvery(LOAD_TICKETS, loadTickets);
  yield takeEvery(LOAD_NEWS, loadNews);
  yield takeEvery(LOAD_ONE_NEWS, loadOneNews);
  yield takeEvery(CREATE_RESERVATION, createReservation);
  yield takeEvery(SAVE_RESERVATION, saveReservation);
  yield takeEvery(DELETE_RESERVATION, deleteReservation);
  yield takeEvery(COMPLETE_RESERVATION, completeReservation);
}
