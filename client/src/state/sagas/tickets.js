import { put, select } from 'redux-saga/effects';
import { ticketsFetched } from '../actions/tickets';
import { requestGet } from '../actions/requests';
import { hasTickets } from '../selectors/tickets';

export function* loadTickets(action) {
  const state = yield select();
  const { requestId, forceOverride } = action.payload;

  if (!hasTickets(state) || forceOverride) {
    yield put(requestGet('ticket', {}, requestId, ticketsFetched));
  }
}
