import { put, select } from 'redux-saga/effects';
import { filteredNewsFetched, newsNotFound, oneNewsFetched } from '../actions/news';
import { getNewsByFilter, getOneNewsById } from '../selectors/news';
import { requestGet } from '../actions/requests';

export function* loadNews(action) {
  const { requestId, filters } = action.payload;
  const state = yield select();
  const existingNews = getNewsByFilter(state.news, filters);

  if (!existingNews) {
    yield put(requestGet('news', filters, requestId, filteredNewsFetched));
  }
}

export function* loadOneNews(action) {
  const state = yield select();
  const { id, requestId } = action.payload;
  const existingNews = getOneNewsById(state.news, id);

  if (!existingNews) {
    yield put(requestGet(`news/${id}`, { id }, requestId, oneNewsFetched, newsNotFound));
  } else {
    yield put(oneNewsFetched({ data: existingNews }));
  }
}
