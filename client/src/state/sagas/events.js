import { select, put } from 'redux-saga/effects';
import { eventFetched, eventNotFound, filteredEventsFetched } from '../actions/events';
import { requestGet } from '../actions/requests';
import { getEventsByFilter } from '../selectors/events';

export function* loadEvent(action) {
  const state = yield select();
  const { id, requestId } = action.payload;
  const existingEvent = state.events.byId[id];

  if (!existingEvent) {
    yield put(requestGet(`event/${id}`, { id }, requestId, eventFetched, eventNotFound));
  } else {
    yield put(eventFetched({ data: existingEvent }));
  }
}

export function* loadEvents(action) {
  const { requestId, filters } = action.payload;
  const state = yield select();
  const existingEvents = getEventsByFilter(state.events, filters);

  if (!existingEvents) {
    yield put(requestGet('event', filters, requestId, filteredEventsFetched));
  }
}
