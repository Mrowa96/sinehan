export function getPlacesFromState(state) {
  return state.places.byId;
}

export function getPlaceFromStateById(state, id) {
  return state.places.byId ? state.places.byId[id] : undefined;
}
