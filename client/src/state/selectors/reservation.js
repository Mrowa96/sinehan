export function getRoomFromReservation({ reservation }) {
  if (reservation.place) {
    return reservation.place.room;
  }
}

export function getPlaceFromReservation({ reservation }) {
  return reservation.place;
}

export function getTotalCost({ reservation, tickets }) {
  return Number.parseFloat(
    reservation.tickets
      .reduce((previous, ticket) => previous + ticket.quantity * tickets.byId[ticket.id].price, 0)
      .toFixed(2),
  );
}

export function getTotalTicketsQuantity({ reservation }) {
  return reservation.tickets.reduce((previous, ticket) => previous + ticket.quantity, 0);
}

export function getGroupedSeatsFromReservation({ reservation }) {
  return reservation.seats
    .reduce((groupedSeats, seatConfiguration) => {
      let groupedSeat = groupedSeats.find(existingGroupedSeat => existingGroupedSeat.row === seatConfiguration.row);

      if (!groupedSeat) {
        groupedSeat = {
          row: seatConfiguration.row,
          columns: [seatConfiguration.column],
        };

        groupedSeats.push(groupedSeat);
      } else {
        groupedSeat.columns.push(seatConfiguration.column);
      }

      groupedSeat.columns.sort((a, b) => a - b);

      return groupedSeats;
    }, [])
    .sort((a, b) => a.row - b.row);
}
