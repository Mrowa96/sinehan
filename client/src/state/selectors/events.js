import moment from 'moment';
import { serializeFilters } from '../../utils/serializeFilters';
import { stripEventOutOfPastShows } from '../../utils/stripEventOutOfPastShows';

export function getEventsByFilter(eventsFromState, filter, dateTime = moment().format('YYYY-MM-DD HH:mm')) {
  const dataByFilter = eventsFromState.dataByFilter[serializeFilters(filter)];
  let events;

  if (dataByFilter) {
    events = [];

    dataByFilter.ids.reduce((output, id) => {
      const event = eventsFromState.byId[id];

      if (event) {
        output.push(stripEventOutOfPastShows(event, dateTime));
      }

      return output;
    }, events);
  }

  return events;
}

export function getEventsByFilterForDate(eventsFromState, filter, dateTime = moment().format('YYYY-MM-DD HH:mm')) {
  const filteredEvents = getEventsByFilter(eventsFromState, filter, dateTime);
  let events;

  if (filteredEvents) {
    events = [];

    filteredEvents.reduce((output, event) => {
      const shows = event.shows.find(show => {
        if (moment(show.date).isSame(moment(dateTime, 'YYYY-MM-DD'))) {
          const datesWithTime = show.startTimes.map(time => `${show.date} ${time}`);

          return datesWithTime.find(dateWithTime => moment(dateWithTime, 'YYYY-MM-DD HH:mm').isSameOrAfter(dateTime));
        }

        return false;
      });

      if (shows) {
        output.push(event);
      }

      return output;
    }, events);
  }

  return events;
}

export function getEventFromReservation(state) {
  return stripEventOutOfPastShows(state.events.byId[state.reservation.eventId]);
}

export function getEventById(eventsFromState, id) {
  return stripEventOutOfPastShows(eventsFromState.byId[id]);
}

export function getNotEmptyFilters(state) {
  let filters;

  Object.keys(state.events.filters).forEach(key => {
    if (Object.prototype.hasOwnProperty.call(state.events.filters, key) && state.events.filters[key]) {
      if (!filters) {
        filters = {};
      }

      filters[key] = state.events.filters[key];
    }
  });

  return filters;
}

export function getShowsGroupedByPlace(event) {
  let results = [];

  if (event.shows) {
    results = Object.values(
      event.shows.reduce((data, show) => {
        if (typeof data[show.place.id] === 'undefined') {
          data[show.place.id] = { ...show.place, shows: [] };
        }

        data[show.place.id].shows.push({
          id: show.id,
          date: show.date,
          startTimes: show.startTimes,
        });

        return data;
      }, {}),
    );
  }

  return results;
}
