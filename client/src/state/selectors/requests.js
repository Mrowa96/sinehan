import RequestStatus from '../../models/RequestStatus';

export function getRequestById(requestsFromState, id) {
  return typeof requestsFromState[id] !== 'undefined'
    ? requestsFromState[id]
    : { status: RequestStatus.NOT_INITIALIZED };
}
