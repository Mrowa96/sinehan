const reservationStep = {
  INITIALIZE: 'initialize',
  RESTORE: 'restore',
  SELECT_TICKETS: 'tickets',
  CHOSE_SEATS: 'seats',
  SET_CLIENT_DATA: 'client',
};

export default reservationStep;
