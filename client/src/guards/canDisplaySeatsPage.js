import canDisplayTicketsPage from './canDisplayTicketsPage';

export default function canDisplaySeatsPage({ reservation }) {
  return !!(canDisplayTicketsPage({ reservation }) && reservation.tickets.length);
}
