import { hasTickets } from '../state/selectors/tickets';
import { getEventById } from '../state/selectors/events';

export default function canDisplayRestorePage(state) {
  const { reservation, events } = state;

  return (
    !!(reservation.id && reservation.fromStorage && reservation.eventId) &&
    (!hasTickets(state) || !getEventById(events, reservation.eventId))
  );
}
