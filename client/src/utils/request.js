import axios from 'axios';
import config from '../config';

export function apiGet(endpoint, filters = {}) {
  const excludedFilters = ['id'];
  let url = `${config.apiUrl}/${endpoint}`;

  if (filters && Object.keys(filters).length) {
    url = `${url}?`;

    Object.keys(filters).forEach(filterName => {
      if (Object.prototype.hasOwnProperty.call(filters, filterName) && !excludedFilters.includes(filterName)) {
        url = `${url}${filterName}=${filters[filterName]}&`;
      }
    });

    url = url.slice(0, -1);
  }

  return axios.get(url);
}

export function apiPost(endpoint, data = {}) {
  const url = `${config.apiUrl}/${endpoint}`;

  return axios.post(url, data);
}

export function apiPut(endpoint, data = {}) {
  const url = `${config.apiUrl}/${endpoint}`;

  return axios.put(url, data);
}

export function apiDelete(endpoint) {
  const url = `${config.apiUrl}/${endpoint}`;

  return axios.delete(url);
}
