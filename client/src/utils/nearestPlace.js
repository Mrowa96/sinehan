import { distanceBetweenPoints } from './distanceBetweenPoints';

export function nearestPlaceId(currentCoords, places) {
  const data = places
    .map(place => ({
      id: place.id,
      value: distanceBetweenPoints(
        {
          x: currentCoords.latitude,
          y: currentCoords.longitude,
        },
        {
          x: place.coords.latitude,
          y: place.coords.longitude,
        },
      ),
    }))
    .sort((a, b) => a.value - b.value);

  if (data[0] && data[0].id) {
    return data[0].id;
  }
}
