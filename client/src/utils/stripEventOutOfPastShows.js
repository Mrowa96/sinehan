import moment from 'moment';

export function stripEventOutOfPastShows(event, currentDateTime = moment().format('YYYY-MM-DD HH:mm')) {
  const currentDate = moment(currentDateTime).format('YYYY-MM-DD');

  if (event && !event.id) {
    return event;
  }

  if (event && event.id) {
    const eventClone = JSON.parse(JSON.stringify(event));

    eventClone.shows = eventClone.shows.reduce((showsData, show) => {
      if (moment(show.date).isSame(currentDate)) {
        show.startTimes = show.startTimes.reduce((prevStartTimes, startTime) => {
          const showDateTime = moment(`${currentDate} ${startTime}`, 'YYYY-MM-DD HH:mm').subtract(30, 'minutes');

          if (showDateTime.isSameOrAfter(currentDateTime)) {
            prevStartTimes.push(startTime);
          }

          return prevStartTimes;
        }, []);

        if (show.startTimes.length) {
          showsData.push(show);
        }
      }

      if (moment(show.date).isAfter(currentDate)) {
        showsData.push(show);
      }

      return showsData;
    }, []);

    return eventClone;
  }
}
