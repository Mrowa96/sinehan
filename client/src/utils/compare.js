export function compareObjects(firstObject, secondObject) {
  return JSON.stringify(firstObject) === JSON.stringify(secondObject);
}
