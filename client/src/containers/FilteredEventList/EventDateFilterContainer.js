import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setEventsDateFilter } from '../../state/actions/events';
import { EventDateFilter } from '../../components/FilteredEventList/EventDateFilter/EventDateFilter';

class EventDateFilterContainer extends Component {
  static propTypes = {
    selectedDate: PropTypes.string,
    locked: PropTypes.bool.isRequired,
    setFilter: PropTypes.func.isRequired,
  };

  static defaultProps = {
    selectedDate: undefined,
  };

  onDateChange = date => {
    const { setFilter } = this.props;

    setFilter(date);
  };

  render() {
    const { locked, selectedDate } = this.props;

    return <EventDateFilter onChange={this.onDateChange} selectedDate={selectedDate} locked={locked} />;
  }
}

const mapStateToProps = state => ({
  selectedDate: state.events.filters.date,
});

const mapDispatchToProps = dispatch => ({
  setFilter: date => {
    dispatch(setEventsDateFilter(date));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EventDateFilterContainer);
