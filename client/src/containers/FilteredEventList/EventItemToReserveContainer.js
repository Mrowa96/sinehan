import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import EventItemToReserve from '../../components/FilteredEventList/EventItemToReserve/EventItemToReserve';

class EventItemToReserveContainer extends PureComponent {
  static propTypes = {
    event: PropTypes.object.isRequired,
    selectedPlaceId: PropTypes.number,
    selectedDate: PropTypes.string,
  };

  static defaultProps = {
    selectedPlaceId: undefined,
    selectedDate: undefined,
  };

  render() {
    const { event, selectedPlaceId, selectedDate } = this.props;

    return <EventItemToReserve event={event} selectedDate={selectedDate} selectedPlaceId={selectedPlaceId} />;
  }
}

const mapStateToProps = state => ({
  selectedPlaceId: state.events.filters.placeId,
  selectedDate: state.events.filters.date,
});

export default connect(
  mapStateToProps,
  null,
)(EventItemToReserveContainer);
