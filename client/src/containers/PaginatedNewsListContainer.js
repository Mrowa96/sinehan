import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { loadNews as loadNewsAction } from '../state/actions/news';
import NewsList from '../components/NewsList/NewsList';
import Pagination from '../components/NewsList/Pagination/Pagination';
import Loader from '../components/Loader/Loader';
import { getNewsByFilter, getNewsTotalQuantityByFilter } from '../state/selectors/news';

class PaginatedNewsListContainer extends Component {
  static propTypes = {
    loadNews: PropTypes.func.isRequired,
    news: PropTypes.shape({
      title: PropTypes.string,
      photo: PropTypes.string,
      category: PropTypes.shape({
        name: PropTypes.string,
      }),
      content: PropTypes.string,
      author: PropTypes.shape({
        name: PropTypes.string,
      }),
    }),
    history: PropTypes.shape({
      location: PropTypes.object.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    news: undefined,
  };

  static fetchInitialData({ page }) {
    return loadNewsAction(Symbol('PaginatedNewsListContainer'), {
      limit: 6,
      page: page || 1,
    });
  }

  constructor(props) {
    super(props);

    const { history } = props;
    const queryParams = new URLSearchParams(history.location.search);
    const page = +queryParams.get('page') || 1;

    this.state = {
      activePage: page,
      itemsPerPage: 6,
    };
  }

  componentDidMount() {
    if (!this._hasNews(true)) {
      this._loadNews();
    }
  }

  onPageChange = page => {
    const { history } = this.props;

    this.setState({ activePage: page }, () => {
      history.push({ search: `page=${page}` });
      this._loadNews();
    });
  };

  render() {
    const { activePage, itemsPerPage } = this.state;

    if (!this._hasNews(true)) {
      return <Loader loading={!this._hasNews(true)} />;
    }
    return (
      <>
        <NewsList news={this._news} />
        {this._hasNews() && (
          <Pagination
            activePage={activePage}
            itemsCountPerPage={itemsPerPage}
            totalItemsCount={this._totalQuantity}
            pageRangeDisplayed={6}
            onChange={this.onPageChange}
          />
        )}
      </>
    );
  }

  _hasNews(strict = false) {
    return strict ? !!this._news : !!this._news && !!this._news.length;
  }

  _loadNews() {
    const { loadNews } = this.props;

    loadNews(Symbol('PaginatedNewsListContainer'), this._filters);
  }

  get _filters() {
    const { itemsPerPage, activePage } = this.state;

    return {
      limit: itemsPerPage,
      page: activePage,
    };
  }

  get _news() {
    const { news } = this.props;

    return getNewsByFilter(news, this._filters);
  }

  get _totalQuantity() {
    const { news } = this.props;

    return getNewsTotalQuantityByFilter(news, this._filters);
  }
}

const mapStateToProps = ({ news }) => ({
  news,
});

const mapDispatchToProps = dispatch => ({
  loadNews: (requestId, filters) => {
    dispatch(loadNewsAction(requestId, filters));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(PaginatedNewsListContainer),
);
