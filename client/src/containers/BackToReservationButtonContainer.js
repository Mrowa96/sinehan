import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import BackToReservationButton from '../components/BackToReservationButton/BackToReservationButton';
import routes from '../routes';
import { getRouteByReservationStep } from '../utils/routeByReservationStep';
import canDisplayRestorePage from '../guards/canDisplayRestorePage';

// TODO Test it
class BackToReservationButtonContainer extends Component {
  static propTypes = {
    reservation: PropTypes.shape({
      eventId: PropTypes.number,
      lastVisitedStep: PropTypes.string,
    }),
    shouldRedirectToRestorePage: PropTypes.bool.isRequired,
    history: PropTypes.shape({
      location: PropTypes.object.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    reservation: undefined,
  };

  render() {
    return <>{this._shouldDisplayButton ? <BackToReservationButton route={this._reservationRoute} /> : null}</>;
  }

  get _shouldDisplayButton() {
    const { reservation, history } = this.props;

    return reservation.id && !history.location.pathname.startsWith(`${routes.reservation.path}/`);
  }

  get _reservationRoute() {
    const { reservation, shouldRedirectToRestorePage } = this.props;

    if (shouldRedirectToRestorePage) {
      return routes.reservationRestore;
    }
    const route = getRouteByReservationStep(routes, reservation.lastVisitedStep);

    if (!route) {
      return routes.reservationClient;
    }

    return route;
  }
}

const mapStateToProps = state => ({
  reservation: state.reservation,
  shouldRedirectToRestorePage: state.reservation && state.reservation.fromStorage && canDisplayRestorePage(state),
});

export default withRouter(
  connect(
    mapStateToProps,
    null,
  )(BackToReservationButtonContainer),
);
