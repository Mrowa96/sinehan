import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  clearReservation as clearReservationAction,
  createReservation as createReservationAction,
} from '../../state/actions/reservation';
import Initialize from '../../components/Reservation/Initialize/Initialize';
import { getRequestById } from '../../state/selectors/requests';
import RequestStatus from '../../models/RequestStatus';

class InitializeContainer extends Component {
  static propTypes = {
    reservation: PropTypes.shape({
      showId: PropTypes.number,
      startTime: PropTypes.string,
    }).isRequired,
    requests: PropTypes.shape({
      status: PropTypes.string,
    }).isRequired,
    createReservation: PropTypes.func.isRequired,
    clearReservation: PropTypes.func.isRequired,
    displayTime: PropTypes.number,
    history: PropTypes.shape({
      goBack: PropTypes.func.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    displayTime: 2000,
  };

  #requestId = Symbol('InitializeContainer');

  componentDidMount() {
    const { createReservation, reservation, displayTime, history } = this.props;

    if (reservation.showId && reservation.startTime) {
      setTimeout(() => {
        createReservation(reservation.showId, reservation.startTime, this.#requestId);
      }, displayTime);
    } else {
      history.goBack();
    }
  }

  componentDidUpdate(prevProps) {
    const { requests, clearReservation } = this.props;
    const prevRequest = this.getRequest(prevProps.requests);
    const currentRequest = this.getRequest(requests);

    if (prevRequest.status !== currentRequest.status && currentRequest.status === RequestStatus.FAIL) {
      // TODO Notify user about error
      clearReservation();
    }
  }

  getRequest = requests => getRequestById(requests, this.#requestId);

  render() {
    return <Initialize />;
  }
}

const mapStateToProps = ({ reservation, requests }) => ({
  reservation,
  requests,
});

const mapDispatchToProps = dispatch => ({
  createReservation: (showId, startTime, requestId) => {
    dispatch(createReservationAction(showId, startTime, requestId));
  },
  clearReservation: () => {
    dispatch(clearReservationAction());
  },
});

export const InitializeConnected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(InitializeContainer);

export default withRouter(InitializeConnected);
