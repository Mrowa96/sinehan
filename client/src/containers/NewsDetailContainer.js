import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { loadOneNews as loadOneNewsAction } from '../state/actions/news';
import NewsDetail from '../components/NewsDetail/NewsDetail';
import Loader from '../components/Loader/Loader';
import { getOneNewsById } from '../state/selectors/news';

class NewsDetailContainer extends Component {
  static propTypes = {
    loadOneNews: PropTypes.func.isRequired,
    news: PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      photo: PropTypes.string,
      category: PropTypes.shape({
        name: PropTypes.string,
      }),
      content: PropTypes.string,
      author: PropTypes.shape({
        name: PropTypes.string,
      }),
    }),
    updateMeta: PropTypes.bool,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    news: undefined,
    updateMeta: true,
  };

  static fetchInitialData = ({ id }) => loadOneNewsAction(id, Symbol('NewsDetailContainer'));

  componentDidMount() {
    const { loadOneNews } = this.props;

    if (!this._hasNews()) {
      loadOneNews(this._id, Symbol('NewsDetailContainer'));
    }
  }

  render() {
    const { news, updateMeta } = this.props;

    if (!this._hasNews()) {
      return <Loader loading={!this._hasNews()} />;
    }

    return <NewsDetail news={news} updateMeta={updateMeta} />;
  }

  _hasNews() {
    const { news } = this.props;

    return !!news;
  }

  get _id() {
    const { match } = this.props;

    return match.params.id;
  }
}

const mapStateToProps = (state, { match }) => ({
  news: getOneNewsById(state.news, match.params.id),
});

const mapDispatchToProps = dispatch => ({
  loadOneNews: (id, requestId) => {
    dispatch(loadOneNewsAction(id, requestId));
  },
});

export const NewsDetailConnected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewsDetailContainer);

export default withRouter(NewsDetailConnected);
