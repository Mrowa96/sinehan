import React, { Component } from 'react';
import NewsDetailContainer from '../containers/NewsDetailContainer';

export default class NewsDetailPage extends Component {
  static fetchInitialData = ({ params }) => [NewsDetailContainer.fetchInitialData(params)];

  render() {
    return <NewsDetailContainer />;
  }
}
