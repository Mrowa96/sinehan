import React, { Component } from 'react';
import PaginatedNewsListContainer from '../containers/PaginatedNewsListContainer';

export default class NewsListPage extends Component {
  static fetchInitialData = ({ query }) => [PaginatedNewsListContainer.fetchInitialData(query)];

  render() {
    return <PaginatedNewsListContainer />;
  }
}
