import React from 'react';
import SeatsManagerContainer from '../../containers/Reservation/SeatsManagerContainer';
import ReservationPageWrapper from '../../components/ReservationPageWrapper/ReservationPageWrapper';

export default function SeatsPage() {
  return <ReservationPageWrapper WrappedComponent={SeatsManagerContainer} />;
}
