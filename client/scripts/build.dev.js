process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

process.on('unhandledRejection', (err) => {
  throw err;
});

const chalk = require('chalk');
const webpack = require('webpack');
const checkRequiredFiles = require('react-dev-utils/checkRequiredFiles');
const config = require('../config/webpack.config.dev');
const paths = require('../config/paths');

if (!checkRequiredFiles([paths.appHtml, paths.appIndexJs])) {
  process.exit(1);
}

console.log(chalk.blue('[CLIENT] Creating development build...\n'));

const compiler = webpack(config);

compiler.watch({}, (err, stats) => {
  console.log(
    stats.toString({
      assets: true,
      children: false,
      entrypoints: false,
      chunks: false,
      colors: true,
      performance: false,
      usedExports: false,
      modules: false,
    }),
  );
});
