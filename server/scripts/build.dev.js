process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

process.on('unhandledRejection', (err) => {
  throw err;
});

const chalk = require('chalk');
const webpack = require('webpack');
const config = require('../config/webpack.config');

console.log(chalk.blue('[SERVER] Creating development build...\n'));

const compiler = webpack(config);

compiler.watch({}, (err, stats) => {
  console.log(
    stats.toString({
      assets: true,
      children: false,
      entrypoints: false,
      chunks: false,
      colors: true,
      performance: false,
      usedExports: false,
      modules: false,
    }),
  );
});
