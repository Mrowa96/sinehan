import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, createStore } from 'redux';
import reducers from '../../client/src/state/reducers';
import { initialState } from '../../client/src/state/store/initialState';
import sagas from '../../client/src/state/sagas';

export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(reducers, initialState, applyMiddleware(sagaMiddleware));

  sagaMiddleware.run(sagas);

  return store;
}
