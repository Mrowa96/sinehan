const path = require('path');

module.exports = function(api) {
  const isDevelopment = api.env() === 'development';
  const isProduction = api.env() === 'production';
  const isTest = api.env() === 'test';
  const presets = [
    !isTest
      ? '@babel/preset-env'
      : [
          '@babel/preset-env',
          {
            targets: {
              node: 'current',
            },
          },
        ],
    [
      '@babel/preset-react',
      {
        development: isDevelopment || isTest,
        useBuiltIns: true,
      },
    ],
  ];
  const plugins = [
    [
      '@babel/plugin-transform-destructuring',
      {
        useBuiltIns: true,
      },
    ],
    [
      '@babel/plugin-proposal-class-properties',
      {
        loose: true,
      },
    ],
    [
      '@babel/plugin-proposal-object-rest-spread',
      {
        useBuiltIns: true,
      },
    ],
    [
      '@babel/plugin-transform-runtime',
      {
        corejs: false,
        helpers: true,
        regenerator: true,
        useESModules: false,
        absoluteRuntime: path.dirname(require.resolve('@babel/runtime/package.json')),
      },
    ],
  ];

  if (isProduction) {
    plugins.push([
      'babel-plugin-transform-react-remove-prop-types',
      {
        removeImport: true,
      },
    ]);
  }

  return {
    presets,
    plugins,
  };
};
